﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/blendShader"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _MainTex2 ("Blend Texture", 2D) = "" {}
        _Blending("Blend Value", Range(0,1)) = 0.0
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        LOD 200

        Pass{
            
            //Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM

            sampler2D _MainTex;
            sampler2D _MainTex2;
            float _Blending;
            // Physically based Standard lighting model, and enable shadows on all light types
            //#pragma surface surf Standard fullforwardshadows

            // Use shader model 3.0 target, to get nicer looking lighting
            //#pragma target 3.0

            #pragma vertex vert
            #pragma fragment frag

            # include "UnityCG.cginc"

            struct VertInput{
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
            };
            struct VertOutput {
                float4 pos: SV_POSITION;
                half3 color : COLOR;
                float2 uv : TEXCOORD0;
            };
            
            VertOutput vert(VertInput i) {
                VertOutput o;
            
                o.pos = UnityObjectToClipPos(i.pos);
                float3 wpos = mul(unity_ObjectToWorld, i.pos).xyz;
                //o.color = i.pos.xyz;
                o.color = wpos;
                o.uv = i.uv;
                return o;
            }
            
            //half4 frag(VertOutput i) : COLOR{
            //    return half4(i.color ,1.0f);
            //}

            float4 frag(VertOutput i) : SV_Target
            {
                float4 color1 = tex2D(_MainTex, i.uv);
                float4 color2 = tex2D(_MainTex2, i.uv);

                
                float alpha = ((1 - i.color.g)*8 ) -4 + ((_Blending * 8)-4 );
                alpha = clamp(alpha, 0, 1);
                float4 color = (color1* alpha) +( color2 * (1- alpha));
                //return float4(i.color, 1);
                return float4(color.r , color.g, color.b, 1);
            }

            

            struct Input
            {
                float2 uv_MainTex;
            };

            half _Glossiness;
            half _Metallic;
            fixed4 _Color;

            // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
            // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
            // #pragma instancing_options assumeuniformscaling
            //UNITY_INSTANCING_BUFFER_START(Props)
                // put more per-instance properties here
            //UNITY_INSTANCING_BUFFER_END(Props)

            //void surf(Input IN, inout SurfaceOutputStandard o)
            //{
            //    // Albedo comes from a texture tinted by color
            //    fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
            //    fixed4 sourceTex = tex2D(_MainTex, IN.uv_MainTex) * (1 - _Blending);
            //    fixed4 targetTex = tex2D(_MainTex2, IN.uv_MainTex) * _Blending;
            //    fixed4 blendedTex = (sourceTex + targetTex) * _Color;
            //    o.Albedo = blendedTex.rgb;
            //    // Metallic and smoothness come from slider variables
            //    o.Metallic = _Metallic;
            //    o.Smoothness = _Glossiness;
            //    //o.Alpha = c.a;
            //    o.Alpha = _Blending;
            //}
            ENDCG
        }

        //Pass 
        //{
        //    CGPROGRAM
        //
        //    //#pragma surface surf Standard fullforwardshadows
        //
        //    // Use shader model 3.0 target, to get nicer looking lighting
        //    //#pragma target 3.0
        //    half _Glossiness;
        //    half _Metallic;
        //    fixed4 _Color;
        //
        //    UNITY_INSTANCING_BUFFER_START(Props)
        //    // put more per-instance properties here
        //    UNITY_INSTANCING_BUFFER_END(Props)
        //
        //    void surf(Input IN, inout SurfaceOutputStandard o)
        //    {
        //        // Albedo comes from a texture tinted by color
        //        fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
        //        fixed4 sourceTex = tex2D(_MainTex, IN.uv_MainTex) * (1 - _Blending);
        //        fixed4 targetTex = tex2D(_MainTex2, IN.uv_MainTex) * _Blending;
        //        fixed4 blendedTex = (sourceTex + targetTex) * _Color;
        //        o.Albedo = blendedTex.rgb;
        //        // Metallic and smoothness come from slider variables
        //        o.Metallic = _Metallic;
        //        o.Smoothness = _Glossiness;
        //        //o.Alpha = c.a;
        //        o.Alpha = _Blending;
        //    }
        //
        //    ENDCG
        //
        //}
        
        
    }
    //FallBack "Diffuse"
}