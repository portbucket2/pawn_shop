﻿using UnityEngine;

[CreateAssetMenu(fileName ="ShopItemCard", menuName ="Create A Shop Item")]
public class ShopItem : ScriptableObject
{
    public Sprite sprite;
    public ShoppingMethod shoppingMethod;
    public int itemPrice;
}
