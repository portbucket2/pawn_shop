﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;

public class CheckShopObjects : MonoBehaviour
{
    public List<GameObject> shopDecoretedItems;

    private void Awake()
    {
        for (int i = 0; i < shopDecoretedItems.Count; i++)
        {
            shopDecoretedItems[i].SetActive(PlayerPrefsX.GetBool(shopDecoretedItems[i].name));
        }
    }

    public void RefreshShopWithNewPurchaseItems()
    {
        for (int i = 0; i < shopDecoretedItems.Count; i++)
        {
            bool result = PlayerPrefsX.GetBool(shopDecoretedItems[i].name);

            if (result != shopDecoretedItems[i].activeSelf)
            {
                shopDecoretedItems[i].SetActive(result);
                shopDecoretedItems[i].transform.localScale = Vector3.zero;
                shopDecoretedItems[i].transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBounce);
            }
        }        
    }
}
