﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarOnRoad : MonoBehaviour
{
    public float carSpeed;
    public bool person;
    public GameObject[] chars;
    public RuntimeAnimatorController walkAnim;

    public GameObject[] cars;
    // Start is called before the first frame update
    void Start()
    {
        CharChange();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, carSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "carBoundary")
        {
            Vector3 newLocalPos = other.GetComponent<CarPathBoundary>().otherBoundary.transform.localPosition ;
            transform.localPosition = new Vector3(transform.localPosition.x , transform.localPosition.y , newLocalPos.z);
            transform.position += transform.forward * 2;

            CharChange();
        }
    }

    public void CharChange()
    {
        if (person)
        {
            int p = Random.Range(0, chars.Length);
            for (int i = 0; i < chars.Length; i++)
            {
                chars[i].SetActive(false);
                if (i == p)
                {
                    chars[i].SetActive(true);
                    chars[i].GetComponent<Animator>().runtimeAnimatorController = walkAnim;
                }
            }
            for (int i = 0; i < cars.Length; i++)
            {
                cars[i].SetActive(false);
                
            }
        }

        if (!person)
        {
            int p = Random.Range(0, cars.Length);
            for (int i = 0; i < cars.Length; i++)
            {
                cars[i].SetActive(false);
                if (i == p)
                {
                    cars[i].SetActive(true);
                    //cars[i].GetComponent<Animator>().runtimeAnimatorController = walkAnim;
                }
            }
        }
    }
}
