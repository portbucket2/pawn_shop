﻿using System.Collections;
using System.Collections.Generic;
using UniBliss;
using UnityEngine;

public class GameManagerForPlugins : MonoBehaviour
{
    float firstBootTime = 0f;
    public static float FirstBootTime { get { return instance == null ? 0 : instance.firstBootTime; } }
    public static int CurrentLevel { get { return GetCurrentLevel(); } }
    
    static GameManagerForPlugins instance;
    public static int TotalLevelCompletedSoFar
    {
        get
        {
            if (instance == null)
            {
                return 0;
            }
            else
            {
                var totalLvCompletedSoFarHD = new HardData<int>("GAME_TOTAL_LEVEL_COMPLETED_SOFAR", 0);
                return totalLvCompletedSoFarHD.value;
            }
        }
    }

    public static void SetTotalLevelCompletedSoFar(int lvCompleted)
    {
        var totalLvCompletedSoFarHD = new HardData<int>("GAME_TOTAL_LEVEL_COMPLETED_SOFAR", 0);
        totalLvCompletedSoFarHD.value = lvCompleted;
    }

    static int GetCurrentLevel()
    {
        //todo
        return GameManagement.instance.levelIndex + 1;
        //return 0;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (instance.gameObject != gameObject)
            {
                DestroyImmediate(this);
            }
        }
        firstBootTime = Time.time;
    }
}
