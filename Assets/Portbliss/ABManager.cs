﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniBliss;

namespace Portbliss.Ad
{
    public enum ABtype
    {
        rv_name_start = 0,
        rv_name_freq = 1
    }

    public static class ABManager
    {
        private static void Init()
        {
            _allSettings = new Dictionary<ABtype, HardAB>();
            _allSettings.Add(ABtype.rv_name_start, new HardAB("rv_name_start"));
            _allSettings.Add(ABtype.rv_name_freq, new HardAB("rv_name_freq"));
        }

        private static Dictionary<ABtype, HardAB> _allSettings;
        public static Dictionary<ABtype, HardAB> allSettings
        {
            get
            {
                if (_allSettings == null) Init();
                return _allSettings;
            }
        }

        public static string GetValueString(ABtype type)
        {
            return allSettings[type].GetValue();
        }

        public static int GetValueInt(ABtype type)
        {
            var s = allSettings[type].GetValue();
            int result = 0;
            int.TryParse(s, out result);
            return result;
        }

        public static float GetValueFloat(ABtype type)
        {
            var s = allSettings[type].GetValue();
            float result = 0f;
            float.TryParse(s, out result);
            return result;
        }

        public static long GetValueLong(ABtype type)
        {
            var s = allSettings[type].GetValue();
            long result = 0;
            long.TryParse(s, out result);
            return result;
        }

        public static bool GetValueBool(ABtype type)
        {
            var s = allSettings[type].GetValue();
            bool result = false;
            bool.TryParse(s, out result);
            return result;
        }


        public class HardAB
        {
            string id;
            string key;
            HardData<string> data;

            public HardAB(string id, string editorDefaultValue = "")
            {
                this.id = id;
                key = string.Format("AB_KEY_{0}", id);
#if UNITY_EDITOR
                data = new HardData<string>(key, editorDefaultValue);
#else
            data = new HardData<string>(key, "");
#endif
            }

            public string GetID()
            {
                return id;
            }
            public void Assign_IfUnassigned(string i)
            {
                if (data.value == "")
                {
                    data.value = i;
                    if (data.value != "") AnalyticsControllerOriginal.LogEventABTesting(id, data.value);
                }
            }

            public string GetValue()
            {
                if (data.value == "")
                {
                    AnalyticsControllerOriginal.LogEventABTesting(id, data.value);
                    GameUtil.LogYellow("AB value was set to default for key " + id, true);
                }
                return data.value;
            }
        }
    }
}