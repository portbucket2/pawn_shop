﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.Ad
{
    public class GDPR_SettingsButtonControl : MonoBehaviour
    {
        [SerializeField] Button gdprButtonInSettings;
        void Start()
        {
            if (AdController.instance != null)
            {
                if (GDPRController.instance.ShowAnnoyance.value == 0 || GDPRController.instance.ShowAnnoyance.value == 1)
                {
                    ActivateButtonForGDPR();
                }
            }
        }

        void ActivateButtonForGDPR()
        {

            gdprButtonInSettings.gameObject.SetActive(true);
            gdprButtonInSettings.onClick.AddListener(() =>
            {

                GDPRController.instance.StartGDPRFromSettings();
            });
        }
    }
}