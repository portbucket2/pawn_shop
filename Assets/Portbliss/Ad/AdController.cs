﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;

namespace Portbliss.Ad
{
    public delegate void OnCompleteAdFuc(bool success);
    public partial class AdController : MonoBehaviour
    {
        const bool logEnabled = false;
        public static AdController instance;
        const string sdkKey = "Lzi5VR_J50y55PM5ctwAwALT5d9g1CKMhT1TF0naOa4fSUn98Vd6rXsvAp4I3A-5LaPvNk4RSvKe5fesxKhRzh";
        static bool isSDKready = false;
        public static bool IsSDK_Ready { get { return isSDKready; } }
#if UNITY_ANDROID
        const string interstitialAdUnitId = "497c38f2214148ca";
#elif UNITY_IOS
        const string interstitialAdUnitId = "";
#else
        const string interstitialAdUnitId = "";
#endif

#if UNITY_ANDROID
        const string rewardedAdUnitId = "4c2f3dca7c5fb5bc";
#elif UNITY_IOS
        const string rewardedAdUnitId = "";
#else
        const string rewardedAdUnitId = "";
#endif

#if UNITY_ANDROID
        const string bannerAdUnitId = "ca7db8b45b89e632";
#elif UNITY_IOS
        const string bannerAdUnitId = "";
#else
        const string bannerAdUnitId = "";
#endif
        [SerializeField] Color bannerColor = Color.white;
        [SerializeField] bool useInterstitial = true, useRewardedVideoAd = false, useBanner = true;
        [SerializeField] MaxSdk.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
        [SerializeField] bool testModeShowMediationDebugger = false;
        [SerializeField] bool ForceLocationEU_Test = false;
        [SerializeField] GDPRController GDPR_Script;
        OnCompleteAdFuc intersitialDel, rewardedDel;
        static bool hasShownVideoAd = false, hasDismissedVideoAd = false;
        static bool hasShownInterstitial = false, hasDismissedIntersitial = false;
        static HardData<bool> consentTaskDoneFlagHD;
        public static HardData<bool> isEUCountry { get; private set; }
        static bool isShowingBanner = false;
        public static HardData<bool> hasNoAdIAP_PurchasedHD { get; private set; }
        public static bool gdpr_done
        {
            get; private set;
        }
        
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartAdSystem ();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (gameObject != null)
                {
                    DestroyImmediate(gameObject);
                }
                else
                {
                    DestroyImmediate(this);
                }
            }
        }

        public static void CompleteConsentTask(bool consentGiven)
        {
            MaxSdk.SetHasUserConsent(consentGiven);
            consentTaskDoneFlagHD.value = true;
            gdpr_done = true;
        }

        private void OnDisable ()
        {
            GDPR_Script.OnGDPR_UI_Completion -= CompleteConsentTask;
        }

        void StartAdSystem()
        {
            isEUCountry = new HardData<bool>("_IS_EU_COUNTRIES_SD", false);
            hasNoAdIAP_PurchasedHD = new HardData<bool>("_NO_AD_IAP_PURCHASED", false);
            consentTaskDoneFlagHD = new HardData<bool>("CONSENT_TASK_DONE_FLAG", false);
            
#if UNITY_IOS || UNITY_ANDROID
            gdpr_done = false;
            GDPR_Script.OnGDPR_UI_Completion += CompleteConsentTask;
            isShowingBanner = false;
            isSDKready = false;
            MaxSdk.SetVerboseLogging(true);
            MaxSdk.SetSdkKey(sdkKey);
            MaxSdk.InitializeSdk();
            AdvertisementWrapper.InitSystem();

            
            /*
            MaxSdkCallbacks.OnVariablesUpdatedEvent += () =>
            {
                //effective, use these A/B code, not the other one
                Debug.Log("A/B test call back fired!");
                foreach (var s in ABManager.allSettings)
                {
                    string varName = s.Value.GetID();
                    string varValue = MaxSdk.VariableService.GetString(varName);
                    GameUtil.LogYellow("variable name: " + varName + " and variable value: " + varValue);
                    if (string.IsNullOrEmpty(varValue) == false)
                    {
                        s.Value.Assign_IfUnassigned(varValue);
                    }
                }
                //string v1 = MaxSdk.VariableService.GetString("ad_start");
                //string v2 = MaxSdk.VariableService.GetString("ad_frequency");
                //GameUtil.LogGreen("on event callback-value1: " + v1 + " and value2: " + v2);
                
            };
            */
            
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
                InitializeAndPreloadSelectedAdTypes();
                // AppLovin SDK is initialized, start loading ads
                isSDKready = true;

                if ( consentTaskDoneFlagHD.value == false )
                {
                    if ( sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies || ForceLocationEU_Test )
                    {
                        isEUCountry.value = true;
                        if (GDPR_Script == null)
                        {
                            consentTaskDoneFlagHD.value = true;
                            gdpr_done = true;
                        }
                        else
                        {
                            // Show user consent dialog
                            GDPR_Script.StartGDPRFromWelcome();
                        }
                    }
                    else if ( sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply )
                    {
                        isEUCountry.value = false;
                        // No need to show consent dialog, proceed with initialization
                        consentTaskDoneFlagHD.value = true;
                        gdpr_done = true;
                    }
                    else
                    {
                        // Consent dialog state is unknown. Proceed with initialization, but check if the consent
                        // dialog should be shown on the next application initialization
                        consentTaskDoneFlagHD.value = false;
                        gdpr_done = true;
                    }
                }
                else
                {
                    gdpr_done = true;
                }
                
                

               

                //MaxSdk.VariableService.LoadVariables();
                //GameUtil.LogYellow("implicit A/B variables");

                /*
                foreach (var s in ABManager.allSettings)
                {
                    string varName = s.Value.GetID();
                    string varValue = MaxSdk.VariableService.GetString(varName);
                    GameUtil.LogYellow("variable name: " + varName + " and variable value: " + varValue);
                    if (string.IsNullOrEmpty(varValue) == false)
                    {
                        s.Value.Assign_IfUnassigned(varValue);
                    }
                }
                //string value1 = MaxSdk.VariableService.GetString("ad_start");
                //string value2 = MaxSdk.VariableService.GetString("ad_frequency");
                //GameUtil.LogGreen("direct call-value1: " + value1 + " and value2: " + value2);
                
                */
                if (testModeShowMediationDebugger)
                {
                    MaxSdk.ShowMediationDebugger();
                }

                Portbliss.Ad.AdController.ShowBanner();
            };

            
#endif
        }

        void InitializeAndPreloadSelectedAdTypes()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (useInterstitial)
            {
                InitializeInterstitialAds();
            }

            if (useRewardedVideoAd)
            {
                InitializeRewardedAds();
            }

            if (useBanner)
            {
                InitializeBannerAds();
            }
#endif
        }

        public static void ShowInterstitialAd(OnCompleteAdFuc OnComplete)
        {
#if UNITY_IOS || UNITY_ANDROID
            hasShownInterstitial = hasDismissedIntersitial = false;
            if (MaxSdk.IsInterstitialReady(interstitialAdUnitId) && isSDKready)
            {
                MaxSdk.ShowInterstitial(interstitialAdUnitId);
                instance.intersitialDel = OnComplete;
            }
            else
            {
                instance.intersitialDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif
        }

        public static void ShowRewardedVideoAd(OnCompleteAdFuc OnComplete)
        {
#if UNITY_IOS || UNITY_ANDROID
            hasShownVideoAd = hasDismissedVideoAd = false;
            if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId) && isSDKready)
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
                instance.rewardedDel = OnComplete;
            }
            else
            {
                instance.rewardedDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif
        }

        public static void ShowBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == false)
            {
                isShowingBanner = true;
                MaxSdk.ShowBanner(bannerAdUnitId);
            }
#endif
        }

        public static void HideBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == true)
            {
                isShowingBanner = false;
                MaxSdk.HideBanner(bannerAdUnitId);
            }
#endif
        }
    }
}