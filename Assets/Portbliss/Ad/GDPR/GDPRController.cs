﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;
using System;

namespace Portbliss.Ad
{
    public class GDPRController : MonoBehaviour
    {
        public static GDPRController instance;
        [SerializeField] string NameOfTheGame;

        [SerializeField] HardData<bool> ShowAnal;
         HardData<bool> ShowAd;
        public HardData<int> ShowAnnoyance;

        [SerializeField] Button TopButton;
        [SerializeField] Button BottomButton;
        [SerializeField] Text TopButtonText;
        [SerializeField] Text BottomButtonText;
        [SerializeField] Toggle AnalToggle;
        [SerializeField] Toggle AdToggle;

        [SerializeField] GameObject WelcomePanel;
        [SerializeField] GameObject InfoPanel;
        [SerializeField] GameObject SettingsPanel;
        [SerializeField] GameObject WarningPanel;

        [SerializeField] GameObject GDPRPanel;
        [SerializeField] Button Annoyingbutton;

        public enum PanelState
        {
            WELCOME, INFO, SETTINGS, WARNING
        }

        PanelState currentPanel;
        internal Action<bool> OnGDPR_UI_Completion;
        void Awake ()
        {
            if (instance = this)
            {
                instance = this;
            }
            else
            {
                if (instance.gameObject != gameObject)
                {
                    DestroyImmediate(this);
                }
                
                return;
            }


            ShowAnal = new HardData<bool> ( "ANALYTICS_ENABLED", true );
            ShowAd = new HardData<bool> ( "AD_ENABLED", true );
            ShowAnnoyance = new HardData<int> ( "CONSENT_STATUS", -1 );

            TopButton.onClick.AddListener ( GoToGame );
            BottomButton.onClick.AddListener ( Matbori );
            AdToggle.onValueChanged.AddListener ( AdToggleCallback );
            AnalToggle.onValueChanged.AddListener ( AnalToggleCallback );
            Annoyingbutton.onClick.AddListener ( StartGDPRFromSettings );

            //WelcomeOpen ();

            if ( ShowAnnoyance.value == 0 )
            {
                ToggleGDPRWindow ( false );
                ToggleAnnoyingWindow ( true );
            }
        }

        void GoToGame ()
        {
            //if ( currentPanel == PanelState.WARNING )
            //{
            //    SettingsOpen ();
            //}
            //else
            //{
            //    if ( ShowAnal.value == true && ShowAd.value == true )
            //    {
            //        GoToGameAnyway ( true );
            //    }
            //    else
            //    {
            //        WarningOpen ();
            //    }
            //}

            switch ( currentPanel )
            {
                case PanelState.WELCOME:
                    GoToGameAnyway ( true );
                    break;
                case PanelState.INFO:
                    GoToGameAnyway ( true );
                    break;
                case PanelState.SETTINGS:
                    if ( ShowAnal.value == true && ShowAd.value == true )
                    {
                        GoToGameAnyway ( true );
                    }
                    else
                    {
                        WarningOpen ();
                    }
                    break;
                case PanelState.WARNING:
                    SettingsOpen ();
                    break;
            }
        }

        void Matbori ()
        {
            switch ( currentPanel )
            {
                case PanelState.WELCOME:
                    InfoOpen ();
                    break;
                case PanelState.INFO:
                    SettingsOpen ();
                    break;
                case PanelState.SETTINGS:
                    InfoOpen ();
                    break;
                case PanelState.WARNING:
                    GoToGameAnyway ( false );
                    break;
            }
        }

        void GoToGameAnyway ( bool consented )
        {
            ToggleGDPRWindow ( false );
            OnGDPR_UI_Completion?.Invoke (consented);
            ShowAnnoyance.value = consented ? 1 : 0;

            if ( !consented )
            {
                ToggleAnnoyingWindow ( true );
            }
        }

        void AdToggleCallback ( bool on )
        {
            ShowAd.value = on;
        }

        void AnalToggleCallback ( bool on )
        {
            ShowAnal.value = on;
        }

        void WelcomeOpen ()
        {
            currentPanel = PanelState.WELCOME;
            TopButtonText.text = "Awesome! I support that :)";
            BottomButtonText.text = "Manage Data Settings";
            CloseAllPanels ();
            WelcomePanel.SetActive ( true );
        }

        void InfoOpen ()
        {
            currentPanel = PanelState.INFO;
            TopButtonText.text = "Awesome! I support that :)";
            BottomButtonText.text = "Next";
            CloseAllPanels ();
            InfoPanel.SetActive ( true );
        }

        void SettingsOpen ()
        {
            currentPanel = PanelState.SETTINGS;
            TopButtonText.text = "Accept";
            BottomButtonText.text = "Back";
            AnalToggle.isOn = ShowAnal.value;
            AdToggle.isOn = ShowAd.value;
            CloseAllPanels ();
            SettingsPanel.SetActive ( true );
        }

        void WarningOpen ()
        {
            currentPanel = PanelState.WARNING;
            TopButtonText.text = "Let me fix my settings";
            BottomButtonText.text = "I understand";
            CloseAllPanels ();
            WarningPanel.SetActive ( true );
        }

        void CloseAllPanels ()
        {
            WelcomePanel.SetActive ( false );
            InfoPanel.SetActive ( false );
            SettingsPanel.SetActive ( false );
            WarningPanel.SetActive ( false );
        }

        public void ToggleGDPRWindow ( bool on )
        {
            GDPRPanel.SetActive ( on );
        }

        public void ToggleAnnoyingWindow ( bool on )
        {
            Annoyingbutton.gameObject.SetActive ( on );
        }

        public void StartGDPRFromWelcome ()
        {
            ToggleGDPRWindow ( true );
            ToggleAnnoyingWindow ( false );
            WelcomeOpen ();
        }

        public void StartGDPRFromSettings ()
        {
            
            ToggleGDPRWindow ( true );
            ToggleAnnoyingWindow ( false );
            SettingsOpen ();
        }
    }
}