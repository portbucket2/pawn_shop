﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;
using UnityEngine.UI;

public class MaxPluginTester : MonoBehaviour
{
    [SerializeField] Button bannerShowBtn, bannerHideBtn, interstitialAdShowBtn, rewardedVideoAdShowBtn;
    void OnEnable()
    {
        bannerShowBtn.onClick.RemoveAllListeners();
        bannerShowBtn.onClick.AddListener(() =>
        {
            AdController.ShowBanner();
            GameUtil.LogGreen("banner ad has been shown");
        });

        bannerHideBtn.onClick.RemoveAllListeners();
        bannerHideBtn.onClick.AddListener(() =>
        {
            AdController.HideBanner();
            GameUtil.LogGreen("banner ad has been hidden");
        });

        interstitialAdShowBtn.onClick.RemoveAllListeners();
        interstitialAdShowBtn.onClick.AddListener(() =>
        {
            AdController.ShowInterstitialAd((success)=> {

                if (success)
                {
                    GameUtil.LogGreen("Interstitial ad has been displayed!");
                }
                else
                {
                    GameUtil.LogRed("Interstitial ad has NOT been displayed!");
                }
            });
        });

        rewardedVideoAdShowBtn.onClick.RemoveAllListeners();
        rewardedVideoAdShowBtn.onClick.AddListener(() =>
        {
            AdController.ShowRewardedVideoAd((success)=> {

                if (success)
                {
                    GameUtil.LogGreen("Rewarded ad has been displayed! Player should be rewarded!");
                }
                else
                {
                    GameUtil.LogGreen("Rewarded ad has NOT been displayed! Player should NOT be rewarded!");
                }
            });
        });
    }
}
