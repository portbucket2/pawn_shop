﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniBliss;
using UnityEngine;

namespace Portbliss.Ad
{
    public class AdvertisementWrapper
    {
        internal static AdvertisementWrapper instance;
        float lastTimeInterstitial = 0f, lastTimeRewarded = 0f;
        int lastLevelInterstitial = 0, lastLevelRewarded = 0;
        public delegate void OnAdPlayFunc();
        public static event OnAdPlayFunc OnRewardedAdStartPlay;
        //HardData<int> interstitialAdShowOnLevel;
        static void AdCoreInterstitial(Action<bool> OnComplete)
        {
            instance.lastTimeInterstitial = Time.time;
            instance.lastLevelInterstitial = GameManagerForPlugins.CurrentLevel;
            AdController.ShowInterstitialAd((success) =>
            {
                instance.lastTimeInterstitial = Time.time;
                instance.lastLevelInterstitial = GameManagerForPlugins.CurrentLevel;
                OnComplete?.Invoke(success);
                if (success)
                {
                    //instance.interstitialAdShowOnLevel.value = GameManagerForPlugins.CurrentLevel;
                }
            });
        }

        static void AdCoreRewarded(Action<bool> OnComplete)
        {
            instance.lastTimeRewarded = Time.time;
            instance.lastLevelRewarded = GameManagerForPlugins.CurrentLevel;
            AnalyticsControllerOriginal.SetCurrentLevelForRV_Analytics(GameManagerForPlugins.CurrentLevel);
            OnRewardedAdStartPlay?.Invoke();
            AdController.ShowRewardedVideoAd((success) =>
            {
                instance.lastTimeRewarded = Time.time;
                instance.lastLevelRewarded = GameManagerForPlugins.CurrentLevel;
                OnComplete?.Invoke(success);
            });
        }

        internal static void InitSystem()
        {
            if (instance == null)
            {
                instance = new AdvertisementWrapper();
                instance.lastTimeInterstitial = 0f;
                instance.lastTimeRewarded = 0f;
                instance.lastLevelInterstitial = 0;
                instance.lastLevelRewarded = 0;
                //instance.interstitialAdShowOnLevel = new HardData<int>("_INTERSTITIAL_AD_SHOW_LEVEL_NUM_HD_KEY", 2);
            }
        }

        public static void AdIteration(Action<bool> OnComplete, bool isRestart, bool isInterstitial)
        {
            if (instance == null)
            {
                InitSystem();
            }

            bool willPlayAd = false;

            int adStartNumInterstitial = 3;
            int adStartNumRewarded = 2;// ABManager.GetValueInt(ABtype.rv_name_start);
            int adFrequencyRewarded = 2; //ABManager.GetValueInt(ABtype.rv_name_freq);
            float adFrequencyInterstitial = 15;

            Debug.Log("last interstitial ad play time: " + instance.lastTimeInterstitial +
                " and last rewarded video ad play time: " + instance.lastTimeRewarded + " and current level: " + GameManagerForPlugins.CurrentLevel +
                " first condition: 'Time.time > instance.lastTimeInterstitial + adFrequencyInterstitial': " +
                (Time.time > instance.lastTimeInterstitial + adFrequencyInterstitial) +
                " second condition: 'GameManagerForPlugins.CurrentLevel > adStartNumInterstitial': " + (GameManagerForPlugins.CurrentLevel > adStartNumInterstitial) +
                " third condition: 'Mathf.Abs(GameManagerForPlugins.CurrentLevel - instance.lastLevelRewarded) > adFrequencyRewarded': " +
                (Mathf.Abs(GameManagerForPlugins.CurrentLevel - instance.lastLevelRewarded) > adFrequencyRewarded) +
                 " and forth condition: 'GameManagerForPlugins.CurrentLevel > adStartNumRewarded : '" + (GameManagerForPlugins.CurrentLevel > adStartNumRewarded));

            //todo
            if (isInterstitial)
            {
                if (Time.time > instance.lastTimeInterstitial + adFrequencyInterstitial &&
                    GameManagerForPlugins.CurrentLevel > adStartNumInterstitial)
                {
                    willPlayAd = true;
                }
            }
            else
            {
                willPlayAd = true;
                /*
                if (Mathf.Abs(GameManagerForPlugins.CurrentLevel - instance.lastLevelRewarded) > adFrequencyRewarded &&
                    GameManagerForPlugins.CurrentLevel > adStartNumRewarded)
                {
                    willPlayAd = true;
                }
                */
            }
            

            if (willPlayAd)
            {
                if (isInterstitial)
                {
                    AdCoreInterstitial(OnComplete);
                }
                else
                {
                    AdCoreRewarded(OnComplete);
                }
            }
            else
            {
                if (isInterstitial)
                {
                    OnComplete?.Invoke(false);
                }
                else
                {
                    UiManage.instance.GetRightPanel(10);
                }
            }
        }

        /// <summary>
        /// This is only for user choice acton
        /// This add will instantly play for user demand of varities button
        /// </summary>
        /// <param name="OnComplete"></param>
        /*
        public static void AdIteration(Action<bool> OnComplete)
        {
            if (instance == null)
            {
                InitSystem();
            }

            if (OnComplete != null)
                AdCoreRewarded(OnComplete);
            else
                UiManage.instance.GetRightPanel(10);
        }
        */

    }
}
