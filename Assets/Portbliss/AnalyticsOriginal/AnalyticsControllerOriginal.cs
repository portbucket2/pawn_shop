﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using Portbliss.Ad;

public class AnalyticsControllerOriginal : MonoBehaviour
{

#if UNITY_EDITOR
    static bool logToConsole = true;
#else
    static bool logToConsole = false;
#endif

    static AnalyticsControllerOriginal instance;
    //static bool fbInitDone;
    // Start is called before the first frame update
    void Awake()
    {
        //fbInitDone = false;
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (instance.gameObject != gameObject)
            {
                DestroyImmediate(this);
            }
        }

        
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(() => {
                //fbInitDone = true;
                Debug.Log("activate FB event call");
                FB.ActivateApp();
            });
        }
        else
        {
            //fbInitDone = true;
            // Already initialized, signal an app activation App Event
            Debug.Log("activate FB event call 2");
            FB.ActivateApp();
        }

        MaxSdkCallbacks.OnRewardedAdClickedEvent += OnRVClicked;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent += OnRVDisplayed;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent += OnRVRewarded;
        AdvertisementWrapper.OnRewardedAdStartPlay += OnStartRewardedAd;
        
    }

    string cur_placement_no_RV_Analytics = "";
    int cur_lv_no_RV_Analytics = 1;
    public static void SetCurrentPlacementForRV_Analytics(string placement)
    {
        instance.cur_placement_no_RV_Analytics = placement;
    }

    public static void SetCurrentLevelForRV_Analytics(int level)
    {
        instance.cur_lv_no_RV_Analytics = level;
    }

    private void OnDestroy()
    {
        MaxSdkCallbacks.OnRewardedAdClickedEvent -= OnRVClicked;
        MaxSdkCallbacks.OnRewardedAdDisplayedEvent -= OnRVDisplayed;
        MaxSdkCallbacks.OnRewardedAdReceivedRewardEvent -= OnRVRewarded;
        AdvertisementWrapper.OnRewardedAdStartPlay -= OnStartRewardedAd;
    }

    private void OnRVRewarded(string adUnitIdentifier, MaxSdkBase.Reward reward)
    {
        LogEvent_RewardADCompleted(cur_lv_no_RV_Analytics, cur_placement_no_RV_Analytics);
    }

    private void OnRVDisplayed(string adUnitIdentifier)
    {
        //??
        LogEvent_RewardADStarted(instance.cur_lv_no_RV_Analytics, instance.cur_placement_no_RV_Analytics);
    }

    private void OnRVClicked(string adUnitIdentifier)
    {
        //??
        LogEvent_RewardADClicked(instance.cur_lv_no_RV_Analytics, instance.cur_placement_no_RV_Analytics);
    }

    public static void OnStartRewardedAd()
    {
        //?
    }

    /*
    public static void LogLevelCompleted(int levelnumber, string object_name, string toolName, string levelType)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        Params[AppEventParameterName.ContentID] = "" + object_name;
        Params["tool_name"] = "" + toolName;
        Params["level_type"] = "" + levelType;
        FB.LogAppEvent(AppEventName.AchievedLevel, null, Params);
    }

    public static void LogPurchase(string currencyCode, string amount)
    {
        Dictionary<string, string> purchaseEvent = new Dictionary<string, string>();
        purchaseEvent.Add("af_currency", currencyCode);
        purchaseEvent.Add("af_revenue", amount);
        purchaseEvent.Add("af_quantity", "1");
        AppsFlyer.trackRichEvent("af_purchase", purchaseEvent);
    }

    static void ExampleSendEventOneTimeAppsflyer(string uniqueIdentifierOfEvent, string eventName,
        Dictionary<string, string> AppsFlyerData)
    {
        HardData<bool> hd = new HardData<bool>(uniqueIdentifierOfEvent, false);
        if (hd.value == false)
        {
            AppsFlyer.trackRichEvent(eventName, AppsFlyerData);
            GameUtil.LogMagenta("one time event named '" + eventName + "' with unique identifier '" + uniqueIdentifierOfEvent + "' has just been fired!", logToConsole);
            hd.value = true;
        }
    }

    public static void LogEventNTimesAdShownIfApplicable(int number)
    {
        var isItDoneHD = new HardData<bool>("HAS_" + number + "_TIMES_AD_BEEN_SHOWN", false);
        var adCountHD = new HardData<int>("TOTAL_AD_COUNT_SOFAR", 0);
        adCountHD.value = adCountHD.value + 1;
        if (adCountHD.value == number && isItDoneHD.value == false)
        {
            string eventName = "ads_watched_" + number;
            //AppsFlyer.trackRichEvent(eventName, new Dictionary<string, string>());
            GameUtil.LogBlack("Log event editor-one time log sent for N number of ad shown---total adCount: " +adCountHD.value+ " and number: " + number, logToConsole);
            isItDoneHD.value = true;
        }
    }
    */


    public static void LogEvent_LevelStarted(int levelNo)
    {
        GameUtil.LogBlack("Log event editor-Started level with 'levelNo': " + levelNo, logToConsole);
        //send it to appsflyer or facebook or both or whatever
        WaitForInit(() => {
            var Params = new Dictionary<string, object>();
            Params[AppEventParameterName.Level] = "" + levelNo;
            FB.LogAppEvent("Level_Started", null, Params);
        });
    }

    public static void LogEvent_LevelCompleted(int levelNo)
    {
        GameUtil.LogBlack("Log event editor-Completed level with 'levelNo': " + levelNo, logToConsole);
        //send it to appsflyer or facebook or both or whatever
        WaitForInit(() => {
            var Params = new Dictionary<string, object>();
            Params[AppEventParameterName.Level] = "" + levelNo;
            FB.LogAppEvent(AppEventName.AchievedLevel, null, Params);
        });
    }

    static void LogFB_Ad_RV(int levelNum, string placement, string eventName)
    {
        WaitForInit(() => {
            var Params = new Dictionary<string, object>();
            Params[AppEventParameterName.Level] = "" + levelNum;
            Params["placement"] = "" + placement;
            FB.LogAppEvent(eventName, null, Params);
        });
    }

    static void LogEvent_RewardADClicked(int levelNo, string placementName)
    {
        GameUtil.LogBlack("Log event editor-Rewarded video ad clicked with 'levelNo': " + levelNo + " and 'placementName': " + placementName.ToString(), logToConsole);
        //send it to appsflyer or facebook or both or whatever

        LogFB_Ad_RV(levelNo, placementName, "rv_click");
    }

    static void LogEvent_RewardADStarted(int levelNo, string placementName)
    {
        GameUtil.LogBlack("Log event editor-Rewarded video ad started with 'levelNo': " + levelNo + " and 'placementName': " + placementName.ToString(), logToConsole);
        //send it to appsflyer or facebook or both or whatever

        LogFB_Ad_RV(levelNo, placementName, "rv_start");
    }

    static void LogEvent_RewardADCompleted(int levelNo, string placementName)
    {
        GameUtil.LogBlack("Log event editor-Rewarded video ad completed with 'levelNo': " + levelNo+ " and 'placementName': "+ placementName.ToString(), logToConsole);
        //send it to appsflyer or facebook or both or whatever

        LogFB_Ad_RV(levelNo, placementName, "rv_complete");
    }

    public static void LogEventABTesting(string abType, string abValue)
    {
        GameUtil.LogBlack("Log event editor-AB testing with type '" + abType + "' and value '" + abValue+"'", logToConsole);
        //send it to appsflyer or facebook or both or whatever
        //Debug.Log("<color='blue'>AB value choice made type:" + abType + " and ab value: " + abValue + " in fb manager-before wait method</color>");
        WaitForInit(() => {
            var Params = new Dictionary<string, object>();
            Params["AB value"] = "" + abValue;

            FB.LogAppEvent(abType, null, Params);
            Debug.Log("<color='blue'>AB value choice made type:" + abType + " and ab value: " + abValue + "</color>");
        });
    }

    static void WaitForInit(Action OnComplete)
    {
        instance.StartCoroutine(instance.WaitForInitCOR(OnComplete));
    }

    IEnumerator WaitForInitCOR(Action OnComplete)
    {
        yield return null;
        while (FB.IsInitialized == false)
        {
            yield return null;
        }
        OnComplete?.Invoke();
    }
}
