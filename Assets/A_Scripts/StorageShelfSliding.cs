﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageShelfSliding : MonoBehaviour
{
    public float sideVel;
    public GameObject ShelfHolder;

    public float multi;

    public static StorageShelfSliding instance;

    public bool snapMode;

    public Vector3 snapPos;

    float shelfGap;
    public int numBerOfShelves;

    public bool restricted;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        shelfGap = GetComponent<StorageManager>().shelfGap;
        numBerOfShelves = GetComponent<StorageManager>().ShelfCount;
    }

    // Update is called once per frame
    void Update()
    {

        if (StoragePanelManageMent.instance.StorageOn)
        {
            if ((Mathf.Abs(sideVel) < 0.008f && !Input.GetMouseButton(0) && !snapMode) || restricted)
            {
                sideVel = 0;
                snapMode = true;
                restricted = false;

                snapPos = new Vector3((float)((int)((ShelfHolder.transform.localPosition.x - (shelfGap * 0.5f)) / shelfGap)) * shelfGap, 0, 0);
            }

            if (snapMode)
            {
                ShelfHolder.transform.localPosition = Vector3.MoveTowards(ShelfHolder.transform.localPosition, snapPos, Time.deltaTime * 4);
            }
            else
            {
                ShelfHolder.transform.Translate(sideVel * multi, 0, 0);

                sideVel = Mathf.Lerp(sideVel, 0, Time.deltaTime * 2);
            }

            if (ShelfHolder.transform.localPosition.x >= 1 || ShelfHolder.transform.localPosition.x <= -((numBerOfShelves - 1) * shelfGap + 1))
            {
                restricted = true;
            }
        }

        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    ShelfSnap(1);
        //}
        //ShelfHolder.transform.localPosition = new Vector3(Mathf.Clamp(ShelfHolder.transform.localPosition.x, -((numBerOfShelves-1) * shelfGap +1), 2), ShelfHolder.transform.localPosition.y, ShelfHolder.transform.localPosition.z);
        if(ShelfHolder.transform.localPosition.x < -numBerOfShelves * shelfGap)
            ShelfHolder.transform.localPosition = new Vector3(-numBerOfShelves * shelfGap, ShelfHolder.transform.localPosition.y, ShelfHolder.transform.localPosition.z);

    }

    public void SideVelInput(float input)
    {
        if (StoragePanelManageMent.instance.StorageOn)
        {
            sideVel = Mathf.Lerp(sideVel, input, Time.deltaTime * 10);
            snapMode = false;
        }
    }

    public void ShelfSnap(int i)
    {
        snapMode = true;
        snapPos = new Vector3(-i * shelfGap, 0, 0);
    }
    public void ShelfSnapByItemSlot(int i)
    {
        snapMode = true;
        int shelfToDrag = (int)((float)(i) / (float)StorageManager.instance.itemSlotsPerShelf);
        //Debug.Log(shelfToDrag);
        snapPos = new Vector3(-shelfToDrag * shelfGap, 0, 0);
    }
}
