﻿using System;
using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using UnityEngine;

public class FreeMoney : MonoBehaviour
{
    public Animator anim;

    int amount = 500;
    float activatedTimeInMinute = 2;

    private void Awake()
    {
        activatedTimeInMinute *= 60;
    }

    public void ClaimFreeMoney()
    {
        //todo freemoney
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("freemoney");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                AccountsControl.instance.BalanceInceaseBy(500);
                Invoke("ActivateThisAfterTwoMinutes", activatedTimeInMinute);
                anim.SetBool("Hide", true);
            }
        }, false, false);
    }

    private void ActivateThisAfterTwoMinutes()
    {
        anim.SetBool("Hide", false);
    }
}
