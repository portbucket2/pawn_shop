﻿using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using UnityEngine;
using UnityEngine.UI;

public class RewardBoardUi : MonoBehaviour
{
    public static RewardBoardUi instance;
    public GameObject RewardBoard;
    public Transform centerPos;
    public Transform leftPos;
    public Transform tarPos;

    

    public int rewardValue;
    private void Awake()
    {

        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (tarPos)
        {
            RewardBoard.transform.position = Vector3.Lerp(RewardBoard.transform.position, tarPos.position, Time.deltaTime * 5);
            if(Vector3.Distance(RewardBoard.transform.position, tarPos.position) < 0.3f)
            {
                tarPos = null;
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            RewardBoardAppear();
        }
        
    }

    public void RewardBoardAppear()
    {
        tarPos = centerPos;

        Debug.Log("reward");
    }
    public void RewardBoardClose()
    {
        tarPos = leftPos;

        GameStatesControl.instance.NextSellerButton();

        WantedBorad.instance.NextThiefPP();
    }

    public void RewardCollect()
    {
        RewardBoardClose();
        GameManagement.instance.particleSold.Play();
        AccountsControl.instance.BalanceInceaseBy(rewardValue);
    }

    public void Reward4XCollect()
    {
        //todo thief
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("thief");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                RewardBoardClose();
                GameManagement.instance.particleSold.Play();
                AccountsControl.instance.BalanceInceaseBy(rewardValue * 4);
            }

        }, false, false);
    }
    
}
