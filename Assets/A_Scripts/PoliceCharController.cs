﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceCharController : MonoBehaviour
{
    public Animator policeAnim;

    public float movespeed;

    public bool policeEnabled;

    public GameObject LeftExit;

    public static PoliceCharController instance;

    public Vector3 policeRootPos;

    private void Awake()
    {
        instance = this;
        policeRootPos = transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        policeAnim.SetInteger("waiting", 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            EnablePolice();
        }
        
        if (policeEnabled)
        {
            transform.Translate(0, 0, movespeed * Time.deltaTime * 6);

            if(Vector3.Distance(transform.position , LeftExit.transform.position )< 0.5f)
            {
                ResetPolice();
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "charStation")
        {
            movespeed = 0;
            policeAnim.SetTrigger("idle");

            Invoke("ArrestWalk", 0.7f);
        }
    }

    public void EnablePolice()
    {
        policeEnabled = true;
        movespeed = 1;
        policeAnim.SetTrigger("walk");
    }

    void ArrestWalk()
    {
        policeAnim.SetTrigger("policeWalk");
        movespeed = 1;


        if (!GameStatesControl.instance.customerType.Equals(CustomerType.ROBBER))
        {
            WantedBorad.instance.BustedAnimPlay();
            ScannerAttributes.instance.GotStolen = true;
        }
    }

    public void ResetPolice()
    {
        transform.position = policeRootPos;
        movespeed = 0;
        policeEnabled = false;
    }
}
