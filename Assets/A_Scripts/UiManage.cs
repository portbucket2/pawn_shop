﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UiManage : MonoBehaviour
{
    public GameObject[] panelsStates;
    
    public static UiManage instance;

    public Text DollarValueText;


    public Text ResultDealText;

    public Animator rightLeftUpSwipeAnim;

    public GameObject sellingPanelCorrected;

    public Text levelValue;
    private void Awake()
    {
        instance = this;
        
    }
    // Start is called before the first frame update
    void Start()
    {
        UpdateAllValueUi();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetRightPanel(int p)
    {
        for (int i = 0; i < panelsStates.Length; i++)
        {
            panelsStates[i].SetActive(false);
        }

        panelsStates[p].SetActive(true);
    }

    public void CloseAllPanels()
    {
        for (int i = 0; i < panelsStates.Length; i++)
        {
            panelsStates[i].SetActive(false);
        }
    }

    public void UpdateAllValueUi()
    {
        DollarValueText.text = ""+(int)AccountsControl.instance.currentBalanceOldVal ;
        levelValue.text = "" + (GameManagement.instance.levelIndex + 1);
    }

    public void SellingPanelCorrectedEnable()
    {
        sellingPanelCorrected.SetActive(true);
    }
    public void SellingPanelCorrectedDisable()
    {
        sellingPanelCorrected.SetActive(false);
    }


}
