﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimController : MonoBehaviour
{
    public GameObject[] charactersInside;
    public int charInsideIndex;
    public Animator thisAnim;
    public GameObject itemHolder;
    public GameObject moneyHolder;
    public GameObject item;

    //public Sprite ppChar;

    public bool rejected;
    public bool itemSold;

    public CustomerType customerType;
    public int robbingAmount;
    public int vipEstimatedAmount;

    public bool Thief;
    public bool Buyer;

    public ItemToSell.ItemTypes buyingItem;
    public ItemToSell.ItemCondition buyingItemCondition;

    public int idelAnimIndex;
    public int happyAnimIndex;
    public int rejectAnimIndex;

    //public GameObject[] thiefStraps;
    public GameObject thiefStrap;


    bool identified;
    private void Awake()
    {
        if (charactersInside[charInsideIndex].GetComponentInChildren<ItemHolder>().gameObject)
        {
            itemHolder = charactersInside[charInsideIndex].GetComponentInChildren<ItemHolder>().gameObject;
        }
        if (charactersInside[charInsideIndex].GetComponentInChildren<MoneyHolder>().gameObject)
        {
            moneyHolder = charactersInside[charInsideIndex].GetComponentInChildren<MoneyHolder>().gameObject;
        }
        thiefStrap = charactersInside[charInsideIndex].GetComponentInChildren<ThiefStrap>().gameObject;
        CharacterInstantiation();
        ItemInstantiation();

    }
    // Start is called before the first frame update
    void Start()
    {
        //thisAnim = GetComponentInChildren<Animator>();

        //item.transform.position = itemHolder.transform.position;
        //item.transform.rotation = itemHolder.transform.rotation;
        //item = Instantiate(item, itemHolder.transform.position , itemHolder.transform.rotation);
        //item.transform.SetParent(itemHolder.transform);

        //GoIdle();

        //if (charactersInside[charInsideIndex].GetComponentInChildren<ItemHolder>().gameObject)
        //{
        //    itemHolder = charactersInside[charInsideIndex].GetComponentInChildren<ItemHolder>().gameObject;
        //}
        //if (charactersInside[charInsideIndex].GetComponentInChildren<MoneyHolder>().gameObject)
        //{
        //    moneyHolder = charactersInside[charInsideIndex].GetComponentInChildren<MoneyHolder>().gameObject;
        //}



    }

    // Update is called once per frame
    void Update()
    {
        
        if (rejected || itemSold)
        {
            item.transform.SetParent(itemHolder.transform);
            item.transform.localPosition = Vector3.Lerp(item.transform.localPosition, Vector3.zero, Time.deltaTime * 10);

            if (itemSold)
            {
                item.transform.localPosition =  Vector3.zero;
            }
            if(Vector3.Magnitude(item.transform.localPosition) < 0.1f)
            {
                item.transform.localPosition = Vector3.zero;
                item.transform.localRotation = Quaternion.Euler(Vector3.zero);
                rejected = false;
                itemSold = false;
            }
            
        }
        if (!identified)
        {
            //IdentifyBuyerCharAnim();
            //identified = true;
        }
        //IdentifyBuyerCharAnim();
    }

    public void GoIdle()
    {
        thisAnim.SetTrigger("idle");
        thisAnim.SetInteger("waiting", idelAnimIndex);
    }

    public void GoWalk()
    {
        thisAnim.SetTrigger("walk");
        thisAnim.SetInteger("waiting", idelAnimIndex);
    }

    public void ItemOnTable()
    {
        thisAnim.SetTrigger("itemOnTable");
    }
    public void GoHappy()
    {
        thisAnim.SetTrigger("happy");

        thisAnim.SetInteger("accepted", happyAnimIndex);
    }
    public void GoSad()
    {
        thisAnim.SetTrigger("sad");
        thisAnim.SetInteger("rejected", rejectAnimIndex);
    }
    public void GoSayprice()
    {
        thisAnim.SetTrigger("sayPrice");
    }

    public void GoArrested()
    {
        thisAnim.SetTrigger("arrest");
        GetComponent<CharPathFollowing>().turnRightThief = true;

        PoliceCharController.instance.EnablePolice();

        
    }

    public void ResetItemOnHand()
    {
        item.transform.SetParent(itemHolder.transform);
        item.transform.localPosition = Vector3.zero;
        item.transform.localRotation = Quaternion.Euler(Vector3.zero);

        
    }

    public void ItemInstantiation()
    {
        item = Instantiate(item, itemHolder.transform.position, itemHolder.transform.rotation);
        item.transform.SetParent(itemHolder.transform);
    }

    public void IdentifyBuyerCharAnim()
    {
        if (Buyer)
        {
            thisAnim.SetBool("buyer", true);
            //thisAnim.SetInteger("buyerInt", 1);

            //thisAnim.SetFloat("buyerFloat", 1);

            //Debug.Log("Buyer");
        }
        else
        {
            thisAnim.SetBool("buyer", false);
            //thisAnim.SetInteger("buyerInt", 0);
        }
    }
    public void CharacterInstantiation()
    {
        for (int i = 0; i < charactersInside.Length; i++)
        {
            charactersInside[i].SetActive(false);
            if (i == charInsideIndex)
            {
                charactersInside[i].SetActive(true);
                thisAnim = charactersInside[i].GetComponent<Animator>();
                
            }
        }

        
    }

    public void ThiefStrapEnableOrNot()
    {
        //for (int i = 0; i < thiefStraps.Length; i++)
        //{
        //    thiefStraps[i].SetActive(false);
        //    if (i == charInsideIndex && Thief)
        //    {
        //        thiefStraps[i].SetActive(true);
        //    }
        //}

        if (Thief)
        {
            thiefStrap.SetActive(true);
        }
        else
        {
            thiefStrap.SetActive(false);
        }
    }
}

public enum CustomerType
{
    ORDINARY,
    VIP,
    ROBBER,
    THIEF
}
