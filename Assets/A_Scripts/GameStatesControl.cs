﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Portbliss.Ad;
using System;
using UnityEngine.UI;

public class GameStatesControl : MonoBehaviour
{
    public GameObject currentCharacter;
    

    public bool rejectedCurrent;

    [HideInInspector]
    public CustomerType customerType;

    public bool Hasthief;

    public bool BuyerMode;


    [Header("Day End Report Properties")]
    public GameObject dayendReportPanel;
    public GameObject claimButton, videoX3Button;
    public TextMeshProUGUI earningText, costText, profitText;

    [Header("VIP Panel Properties")]
    public GameObject askForVipPanel;

    [Header("Rober Panel Properties")]
    public GameObject askForRobberPanel;
    public Button dontKillMeButton;
    [HideInInspector]
    public int robbingAmount, vipEstimatedAmount;

    public static GameStatesControl instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        askForVipPanel.SetActive(false);
        askForRobberPanel.SetActive(false);
        dayendReportPanel.SetActive(false);
        SellerCharactersMamanger.instance.GetASeller();
        IdleState();
    }

    // Update is called once per frame
    void Update()
    {
        //targetDemo.text = "" + currentCharacter.GetComponent<CharPathFollowing>().targetPoint;
        if (Input.GetKeyDown(KeyCode.A))
        {
            SellItemToBuyer();
        }
    }


    public void IdleState()
    {
        GameManagement.instance.ChangeStateTo(0);
        currentCharacter.GetComponent<CharacterAnimController>().GoIdle();
        ScreenAttributes.instance.AppearPanelIdle();
    }

    public void WalkToDeskState()
    {
        GameManagement.instance.ChangeStateTo(1);
        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        currentCharacter.GetComponent<CharacterAnimController>().GoWalk();

        ScreenAttributes.instance.AppearPanelIdle();

        //DoorAnimControl.instance.OpenDoor();
        Invoke("OpenDoorGameState", 0.7f);
        //OpenDoorGameState();

        GameManagement.instance.LevelStarted();
    }
    public void ReachedDeskState()
    {
        currentCharacter.GetComponent<CharPathFollowing>().moving = false;

        if (customerType.Equals(CustomerType.VIP))
        {
            currentCharacter.GetComponent<charHUD>().EnabeSpeachBubbleFor();

            currentCharacter.GetComponent<CharacterAnimController>().GoIdle();
            askForVipPanel.SetActive(true);
            return;
        }

        if (customerType.Equals(CustomerType.ROBBER))
        {
            currentCharacter.GetComponent<charHUD>().EnabeSpeachBubbleFor();

            currentCharacter.GetComponent<CharacterAnimController>().idelAnimIndex = 2;
            currentCharacter.GetComponent<CharacterAnimController>().GoIdle();
            askForRobberPanel.SetActive(true);

            if(AccountsControl.instance.currentBalance >= robbingAmount)
                dontKillMeButton.enabled = true;
            else
                dontKillMeButton.enabled = false;

            return;
        }

        StartOnDeskActivity();
    }

    public void StartOnDeskActivity()
    {
        if (BuyerMode)
        {
            DecisionState();
        }
        else
        {
            GameManagement.instance.ChangeStateTo(2);
            currentCharacter.GetComponent<CharacterAnimController>().ItemOnTable();


            ScannerAttributes.instance.GrabItemDelayed();
        }
    }

    public void OnWelcomeVIP()
    {
        //todo vip
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("vip");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                currentCharacter.GetComponent<charHUD>().CloseHud();
                askForVipPanel.SetActive(false);
                StartOnDeskActivity();
            }
        }, false, false);
    }

    public void OnDeclineVIP()
    {
        currentCharacter.GetComponent<charHUD>().CloseHud();
        askForVipPanel.SetActive(false);
        currentCharacter.GetComponent<CharacterAnimController>().thisAnim.SetBool("buyer", true);
        ResultStatenegative();
    }

    public void OnRobberMotherFucker()
    {
        //todo robber
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("robber");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                currentCharacter.GetComponent<charHUD>().CloseHud();
                askForRobberPanel.SetActive(false);
                RobberToPolice();
            }
        }, false, false);
    }

    public void OnRobberDontKillMe()
    {
        currentCharacter.GetComponent<charHUD>().CloseHud();
        askForRobberPanel.SetActive(false);
        if (AccountsControl.instance.BalanceDecreaseBy(robbingAmount))
        {
            GoAwayRobber();
        }
    }

    public void ScanningState()
    {
        GameManagement.instance.ChangeStateTo(3);
    }

    public void DecisionState()
    {
        GameManagement.instance.ChangeStateTo(4);
        currentCharacter.GetComponent<charHUD>().AppearSpeechBubble();
        currentCharacter.GetComponent<CharacterAnimController>().GoSayprice();

        if (Hasthief)
        {
            UiManage.instance.rightLeftUpSwipeAnim.SetBool("thiefMode", true);
        }
        else
        {
            UiManage.instance.rightLeftUpSwipeAnim.SetBool("thiefMode", false);
        }
        
    }

    public void ResultStatePositive()
    {
        if (BuyerMode)
        {
            GetStoaragePanel();
        }
        else if (AccountsControl.instance.SpendBoughtValue())
        {
            
            rejectedCurrent = false;

            UiManage.instance.ResultDealText.text = "GREAT DEAL !!!";
            GameManagement.instance.ChangeStateTo(6);
            currentCharacter.GetComponent<charHUD>().RatingHudEnable();
            currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointRight;

            currentCharacter.GetComponent<CharacterAnimController>().GoHappy();

            GameManagement.instance.particleDollars.Play();

            ScannerAttributes.instance.BoughtItem = true;
            DollarBills.instance.DollarForSellerOnScanner();
            Invoke("GoAwayChar", 2f);
            //GoAwayChar();

            //Invoke("GetSellingState",3f);
            Invoke("GetStoaragePanel", 3f);

            if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
            {
                WantedBorad.instance.NextThiefPP();
            }
               
        }
        
    }
    public void ResultStatenegative()
    {

        if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
        {
            WantedBorad.instance.NextThiefPP();
        }
        rejectedCurrent = true;

        UiManage.instance.ResultDealText.text = "REJECTED !";
        GameManagement.instance.ChangeStateTo(6);
        currentCharacter.GetComponent<charHUD>().RatingHudReject();
        currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
        //GoAwayChar();
        currentCharacter.GetComponent<CharacterAnimController>().GoSad();

        Invoke("GoAwayChar", 2f);

        Invoke("NextSellerButton", 3f);
    }

    public void ResultStateThiefToPolice()
    {
        if (currentCharacter.GetComponent<CharacterAnimController>().Thief)
        {


            UiManage.instance.ResultDealText.text = "THIEF !!!";
            GameManagement.instance.ChangeStateTo(6);
            currentCharacter.GetComponent<charHUD>().CloseHud();
            currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
            currentCharacter.GetComponent<CharacterAnimController>().GoArrested();

            PoliceLights.instance.EnablePoliceLights();

            Invoke("GoAwayThief", 2f);

            //Invoke("NextSellerButton", 3f);

        }

    }

    public void RobberToPolice()
    {
        UiManage.instance.ResultDealText.text = "Robber !!!";
        GameManagement.instance.ChangeStateTo(6);
        currentCharacter.GetComponent<charHUD>().CloseHud();
        currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
        currentCharacter.GetComponent<CharacterAnimController>().GoArrested();

        PoliceLights.instance.EnablePoliceLights();

        Invoke("GoAwayRobber", 2f);

    }

    public void GoAwayChar()
    {
        if (rejectedCurrent)
        {
            currentCharacter.GetComponent<CharacterAnimController>().rejected = true;
        }
        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        currentCharacter.GetComponent<CharacterAnimController>().GoWalk();

        Debug.Log("Away");
        SellerCharactersMamanger.instance. GetNextSellerIndex();
    }
    public void GoAwayThief()
    {

        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        RewardBoardUi.instance.RewardBoardAppear();

        Debug.Log("thief Away");
        SellerCharactersMamanger.instance.GetNextSellerIndex();
        //currentCharacter.GetComponent<CharacterAnimController>().GoWalk();
    }

    public void GoAwayRobber()
    {
        currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointLeft;
        currentCharacter.GetComponent<CharPathFollowing>().moving = true;
        currentCharacter.GetComponent<CharacterAnimController>().GoWalk();
        SellerCharactersMamanger.instance.GetNextSellerIndex();

        Invoke("NextSellerButton", 2);
    }

    public void GetSellingState()
    {
        //GameManagement.instance.ChangeStateTo(7);

        UiManage.instance.SellingPanelCorrectedEnable();
        SellingPanel.instance.ResetOfferIndex();
        SellingPanel.instance.UpdateSellingPanelValues();

        if(!( StoragePanelManageMent.instance.storageState == StoragePanelManageMent.StorageState.idle))
        {
            //currentCharacter.GetComponent<CharacterAnimController>().itemSold = true;
            ScreenAttributes.instance.AppearPanelIdle();
        }

        //Destroy(ScannerAttributes.instance.currentItem);

    }

    public void GetSoldState()
    {
        //GameManagement.instance.ChangeStateTo(8);
        UiManage.instance.SellingPanelCorrectedDisable();
        
        if (GameManagement.instance.state == GameManagement.State.result)
        {
            Invoke("NextSellerButton", 1.5f);
        }
        
        //else
        //{
        //    GameManagement.instance.ChangeStateTo(0);
        //}
        AccountsControl.instance.GetProfitIntoBalance();
        GameManagement.instance.particleSold.Play();

    }

    public void NextSellerButton()
    {
        //GameManagement.instance.LevelAccomplished();
        //GameManagement.instance.NextLevel();

        SellerCharactersMamanger.instance.GetNextSeller();

        if (!GameManagement.instance.EndOfTheDay())
            WalkToDeskState();
        else
            ShowDayEndReport();
    }

    public void GetStoaragePanel()
    {
        if (BuyerMode)
        {
            StoragePanelManageMent.instance.storageState = StoragePanelManageMent.StorageState.itemQuery;
            StoragePanelManageMent.instance.TransitionIn();

            //currentCharacter.GetComponent<CharacterAnimController>().itemSold = true;
        }
        else
        {
            StoragePanelManageMent.instance.storageState = StoragePanelManageMent.StorageState.itemAdded;
            StoragePanelManageMent.instance.TransitionIn();

            currentCharacter.GetComponent<CharacterAnimController>().itemSold = true;
        }
        

        ScreenAttributes.instance.AppearPanelIdle();
    }

    public void SellItemToBuyer()
    {
        rejectedCurrent = false;

        UiManage.instance.ResultDealText.text = "GREAT DEAL !!!";
        GameManagement.instance.ChangeStateTo(6);
        currentCharacter.GetComponent<charHUD>().RatingHudEnable();
        currentCharacter.GetComponent<CharPathFollowing>().targetPoint = PathFollowSystem.instance.exitPointRight;

        currentCharacter.GetComponent<CharacterAnimController>().GoHappy();

        GameManagement.instance.particleDollars.Play();

        //AccountsControl.instance.SpendBoughtValue();
        //AccountsControl.instance.GetProfitIntoBalance();
        AccountsControl.instance.BalanceInceaseBy(GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().BuyValueHud);

        //ScannerAttributes.instance.BoughtItem = true;
        Invoke("GoAwayChar", 2.5f);
        Invoke("NextSellerButton", 4f);

        UniversalItem.instance.SendUniversalItemToSeller();

        //GameStatesControl.instance.NextSellerButton();
    }

    public void OpenDoorGameState()
    {
        DoorAnimControl.instance.OpenDoor();
    }

    int earning = 0, cost = 0, profit = 0;
    public void ShowDayEndReport()
    {        
        cost = PlayerPrefs.GetInt("dayendCost");
        earning = PlayerPrefs.GetInt("dayendEarning");
        profit = earning - cost;

        dayendReportPanel.SetActive(true);
        earningText.text = earning.ToString();
        costText.text = cost.ToString();
        profitText.text = profit.ToString();

        if ((earning - cost) < 100) {

            claimButton.SetActive(true);
            videoX3Button.SetActive(false);
        }
        else
        {
            claimButton.SetActive(false);
            videoX3Button.SetActive(true);
        }

    }

    public void OnCloseDayEndReport()
    {
        dayendReportPanel.SetActive(false);
        PlayerPrefs.SetInt("dayendCost", 0);
        PlayerPrefs.SetInt("dayendEarning", 0);
        WalkToDeskState();
    }

    public void OnClaim300ButtonPress()
    {
        //todo dailybonus
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("dailybonus");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                AccountsControl.instance.BalanceInceaseBy(300);
                OnCloseDayEndReport();
            }
        },false,false);
    }

    public void OnVideoX3ButtonPress() {

        //todo dailybonus
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("dailybonus");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                AccountsControl.instance.BalanceInceaseBy(profit * 3);
                OnCloseDayEndReport();
            }
        },false,false);
    }
}
