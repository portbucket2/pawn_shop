﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccountsControl : MonoBehaviour
{
    public int currentBalance;
    public float currentBalanceOldVal;
    public int currentBoughtvalue;
    public int currentEstValue;

    public int lastSoldvalue;
    public static AccountsControl instance;

    public bool balanceUpdating;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        currentBalanceOldVal = currentBalance = GameManagement.instance.dollarsPP;
        UiManage.instance.UpdateAllValueUi();
    }

    // Update is called once per frame
    void Update()
    {
        //if(currentBalanceOldVal < currentBalance)
        //{
        //    balanceUpdating = true;
        //}
        if (balanceUpdating)
        {
            float per = Mathf.MoveTowards(0, 1, Time.deltaTime * 5f);
            currentBalanceOldVal = Mathf.Lerp(currentBalanceOldVal, (float)currentBalance, per);
            
            if (Mathf.Abs(currentBalanceOldVal - (float)currentBalance) < 1f)
            {
                balanceUpdating = false;
                currentBalanceOldVal = currentBalance;
                per = 0;
            }
            UiManage.instance.UpdateAllValueUi();
        }
    }

    public void GetProfitIntoBalance()
    {
        SaveDailyProfit(lastSoldvalue);
        currentBalance += lastSoldvalue;
        UiManage.instance.UpdateAllValueUi();
        balanceUpdating = true;
        GameManagement.instance. DollarsPPSave();
    }
    public bool SpendBoughtValue()
    {        
        // Check if user have enough money
        if (currentBalance >= currentBoughtvalue)
        {

            SaveDailyCost(currentBoughtvalue);
            currentBalance -= currentBoughtvalue;
            //if (currentBalance <= 0)
            //{
            //    currentBalance = 1000;
            //}
            UiManage.instance.UpdateAllValueUi();
            balanceUpdating = true;

            GameManagement.instance.DollarsPPSave();

            return true;
        }
        else // Show you don't have enough money.
        {
            UiManage.instance.GetRightPanel(9);
            return false;
        }
    }

    public void GetDataOfNewItem(GameObject item)
    {
        currentBoughtvalue = item.GetComponent<ItemToSell>().buyValue;
        currentEstValue = item.GetComponent<ItemToSell>().EstValue;

    }
    public void BalanceInceaseBy(int t)
    {
        SaveDailyProfit(t);
        currentBalance += t;
        UiManage.instance.UpdateAllValueUi();
        balanceUpdating = true;

        GameManagement.instance.DollarsPPSave();
    }

    public bool BalanceDecreaseBy(int t)
    {
        // Check if user have enough money
        if (currentBalance >= t)
        {

            SaveDailyCost(t);
            currentBalance -= t;
            //if (currentBalance <= 0)
            //{
            //    currentBalance = 1000;
            //}
            UiManage.instance.UpdateAllValueUi();
            balanceUpdating = true;

            GameManagement.instance.DollarsPPSave();

            return true;
        }
        else // Show you don't have enough money.
        {
            UiManage.instance.GetRightPanel(9);
            return false;
        }
    }

    public void SaveDailyProfit(int value)
    {
        PlayerPrefs.SetInt("dayendEarning", PlayerPrefs.GetInt("dayendEarning", 0) + value);
    }

    public void SaveDailyCost(int value)
    {
        PlayerPrefs.SetInt("dayendCost", PlayerPrefs.GetInt("dayendCost", 0) + value);
    }

}
