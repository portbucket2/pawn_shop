﻿using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using UnityEngine;
using UnityEngine.UI;

public class ShopUpgradePanel : MonoBehaviour
{
    public ShopItem[] shopItemList;
    public GameObject itemPrefab;
    public Transform itemHolder;

    // Start is called before the first frame update
    void Awake()
    {
        for (int i = 0; i < shopItemList.Length; i++)
        {            
            if (!PlayerPrefsX.GetBool(shopItemList[i].sprite.name))
            {
                GameObject go = Instantiate(itemPrefab, itemHolder) as GameObject;
                Transform coinObj = go.transform.GetChild(0);
                Transform videoObj = go.transform.GetChild(1);
                int tempIndex = i;

                coinObj.gameObject.SetActive(shopItemList[i].shoppingMethod.Equals(ShoppingMethod.COIN));
                coinObj.GetChild(0).GetComponent<TMPro.TextMeshProUGUI>().text = shopItemList[i].itemPrice.ToString();
                coinObj.GetComponent<Button>().onClick.AddListener(() => BuyButtonAction(go, tempIndex));

                videoObj.gameObject.SetActive(shopItemList[i].shoppingMethod.Equals(ShoppingMethod.VIDEO));
                videoObj.GetComponent<Button>().onClick.AddListener(() => BuyButtonAction(go, tempIndex));

                go.transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = shopItemList[i].sprite;

            }
        }
    }

    void BuyButtonAction(GameObject go, int i)
    {
        switch (shopItemList[i].shoppingMethod)
        {
            case ShoppingMethod.COIN:
                if (AccountsControl.instance.BalanceDecreaseBy(shopItemList[i].itemPrice))
                {
                    PlayerPrefsX.SetBool(shopItemList[i].sprite.name, true);
                    go.SetActive(false);
                }
                break;
            case ShoppingMethod.VIDEO:
                //todo decor
                AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("decor");
                AdvertisementWrapper.AdIteration((success) =>
                {
                    if (success)
                    {
                        PlayerPrefsX.SetBool(shopItemList[i].sprite.name, true);
                        go.SetActive(false);
                    }
                }, false, false);
                break;
        }
    }

}

public enum ShoppingMethod
{
    VIDEO,
    COIN
}
