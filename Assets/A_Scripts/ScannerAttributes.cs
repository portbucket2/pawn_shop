﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerAttributes : MonoBehaviour
{
    public static ScannerAttributes instance;
    public Transform scanPosition;

    public GameObject currentItem;

    public ParticleSystem ScannerParticle;

    public Transform stolenPlaceHolder;
    public Transform boughtItemPlaceHolder;

    public bool GotItem;

    public bool GotStolen;

    public bool BoughtItem;




    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetItemOnScanner();
    }

    public void GetItemOnScanner()
    {
        if (GotItem)
        {
            currentItem.transform.position = Vector3.Lerp(currentItem.transform.position, scanPosition.position, Time.deltaTime * 10);
            currentItem.transform.rotation = Quaternion.Lerp(currentItem.transform.rotation, scanPosition.rotation, Time.deltaTime * 10);
            if (Vector3.Distance(currentItem.transform.position, scanPosition.position) < 0.005f)
            {
                
                currentItem.transform.position = scanPosition.position;
                currentItem.transform.rotation = scanPosition.rotation;
                ScreenAttributes.instance.AppearPanelScanning();
                ScannerParticle.Play();
                GotItem = false;

                GameStatesControl.instance.ScanningState();

                Invoke("GetScanningDone", 2f);
            }
        }

        if (GotStolen)
        {
            currentItem.transform.position = Vector3.Lerp(currentItem.transform.position, stolenPlaceHolder.position, Time.deltaTime * 10);
            if (Vector3.Distance(currentItem.transform.position, stolenPlaceHolder.position) < 0.1f)
            {
                GotStolen = false;
            }
        }

        if (BoughtItem)
        {
            currentItem.transform.position = Vector3.MoveTowards(currentItem.transform.position, boughtItemPlaceHolder.position, Time.deltaTime * 5);
            if (currentItem.transform.position == boughtItemPlaceHolder.position)
            {
                BoughtItem = false;
            }
        }
    }

    public void GetScanningDone()
    {
        GameStatesControl.instance.DecisionState();
        ScreenAttributes.instance.AppearPanelInfo();

    }

    public void GrabItem()
    {
        GotItem = true;
        currentItem.transform.SetParent(transform);
    }

    public void GrabItemDelayed()
    {
        Invoke("GrabItem", 0.35f);
    }

    public void SendItemToRootPosition()
    {

    }
}
