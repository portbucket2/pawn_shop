﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputSystemMouse : MonoBehaviour
{
    public bool swippingLeft;
    public bool swippingRight;
    public bool swippingUp;
    public Vector3 oldMousePos;
    public Vector3 currentMousePos;
    
    public Vector3 progression;
    

    public float swipeThreshold;

    public bool swiped;

    public Vector3 lastFramePos;
    public Vector3 progressionVelocity;
    public float SideProgressionForShelf;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            oldMousePos = Input.mousePosition;
            lastFramePos = oldMousePos;
        }

        if (Input.GetMouseButton(0))
        {
            progressionVelocity = (Input.mousePosition - lastFramePos)* 10/ Screen.width;
            lastFramePos = Input.mousePosition;
            SideProgressionForShelf = (Input.mousePosition.x - oldMousePos.x) / Screen.width;

            StorageShelfSliding.instance.SideVelInput(progressionVelocity.x);
        }

        if (Input.GetMouseButton(0) && GameManagement.instance.state == GameManagement.State.Decision && !StoragePanelManageMent.instance.StorageOn)
        {
            if (!swiped)
            {
                currentMousePos = Input.mousePosition;

                progression = (currentMousePos - oldMousePos) / Screen.width;

                if (Vector3.Magnitude(progression) > swipeThreshold)
                {
                    if(Mathf.Abs(progression.x) > Mathf.Abs(progression.y))
                    {
                        if (progression.x > 0)
                        {
                            swippingRight = true;
                            swiped = true;
                            RightSwipe();
                        }
                        else
                        {
                            swippingLeft = true;
                            swiped = true;

                            LeftSwipe();
                        }
                    }
                    else
                    {
                        if (progression.y > 0)
                        {
                            swippingUp = true;
                            swiped = true;
                            UpSwipe();
                        }
                    }
                    
                }
            }
            
        }
        else
        {
            progression = Vector3.zero;
            swippingRight = false;
            swippingLeft = false;
            swippingUp = false;
            swiped = false;
        }

        
    }

    public void RightSwipe()
    {
        GameStatesControl.instance.ResultStatePositive();
    }
    public void LeftSwipe()
    {
        GameStatesControl.instance.ResultStatenegative();
    }
    public void UpSwipe()
    {
        GameStatesControl.instance.ResultStateThiefToPolice();
    }
}
