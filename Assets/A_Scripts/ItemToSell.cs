﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemToSell : MonoBehaviour
{
    public string title;
    public string type;
    //public string condition;

    public int buyValue;
    public int EstValue;

    public GameObject visual;

    public Sprite itemImage;

    public int itemId;
    public int itemUniqueIdInShelf;

    public enum ItemTypes
    {
        Ring , Painting , Coin , Statue , Dagger  , Watch, Gun, Antique, Signed, Bone, Jewel
    }
    public enum ItemCondition
    {
        Bad, Good , Excellent
    }
   
    public ItemTypes itemType;
    public ItemCondition itemCond;
    public Sprite[] itemImagesAccToType;
    public Renderer matRend;
    public Material transMatItem;
    public Material myMat;
    public Material nonPolishedMat;
    public Material polishedMat;

    public ScriptableObjCollectionsData itemData;
    public bool polished;
    //public Texture2D myTex;
    // Start is called before the first frame update
    private void Awake()
    {
        //matRend = GetComponentInChildren<Renderer>();
    }
    void Start()
    {
        GameObject go = Instantiate(visual, transform.position, transform.rotation);
        go.transform.SetParent(transform);

        nonPolishedMat = itemData.nonPolishedMats[itemId];
        polishedMat = itemData.polishedMats[itemId];
        matRend = GetComponentInChildren<Renderer>();

        //if (itemCond == ItemCondition.Excellent)
        //{
        //    polished = true;
        //    EstValueAddDueToPolish();
        //
        //}
        polishedOrNonPolishedMaterial();
        MakeItemNonTransparent();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MakeItemTransparent()
    {
        GetComponentInChildren<Renderer>().material = transMatItem;
    }

    public void MakeItemNonTransparent()
    {
        GetComponentInChildren<Renderer>().material = myMat;

    }
    public void polishedOrNonPolishedMaterial()
    {
        if(itemCond == ItemCondition.Excellent)
        {
            myMat = polishedMat;
            if (!polished)
            {
                polished = true;
                EstValueAddDueToPolish();
            }

        }
        else
        {
            myMat = nonPolishedMat;
        }
        //matRend.material = myMat;
        MakeItemNonTransparent();
    }

    public void EstValueAddDueToPolish()
    {
        EstValue = (int)((float)EstValue * 1.2f);
    }
}
