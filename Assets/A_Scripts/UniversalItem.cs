﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalItem : MonoBehaviour
{
    public List<GameObject> itemAlll;
    public Transform rootPos;
    public Transform OnSnannerPos;

    public bool sending;
    public bool takingBuyer;
    public static UniversalItem instance;

    public Transform handTarget;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < StorageManager.instance.itemsCreated.Length; i++)
        {
            GameObject go = Instantiate(StorageManager.instance.itemsCreated[i].gameObject, transform.position, transform.rotation);
            go.transform.SetParent(transform);
            itemAlll.Add(go);
        }

        BackUniversalToRoot();
        //rootPos = transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (sending)
        {
            transform.position = Vector3.Lerp(transform.position, OnSnannerPos.position, Time.deltaTime * 5);
            if(Vector3.Distance(transform.position, OnSnannerPos.position) < 0.01f)
            {
                transform.position = OnSnannerPos.position;
                sending = false;
                Invoke("TransferUniversalItemToBuyer", 1f);
            }
        }

        if (takingBuyer)
        {
            transform.position = Vector3.Lerp(transform.position, handTarget.position, Time.deltaTime * 15);
            if (Vector3.Distance(transform.position, handTarget.position) < 0.1f)
            {
                transform.position = handTarget.position;
                takingBuyer = false;
                this.transform.SetParent(handTarget.transform);
        
            }
        }
    }

    public void SendUniversalItemToSeller()
    {
        for (int i = 0; i < itemAlll.Count; i++)
        {
            itemAlll[i].SetActive(false);
            if(i == StoragePanelManageMent.instance.selectedItemRootIndex)
            {
                itemAlll[i].SetActive(true);
            }
        }
        sending = true;


    }
    

    public void TransferUniversalItemToBuyer()
    {
        //this.transform.SetParent(GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().itemHolder.transform);
        handTarget = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().itemHolder.transform;
        Invoke("BackUniversalToRoot", 1f);
        takingBuyer = true;

        DollarBills.instance.DollarForMeOnScanner();
    }

    public void BackUniversalToRoot()
    {
        sending = false;
        transform.position = rootPos.position;
        transform.rotation = rootPos.rotation;
        transform.SetParent( ScannerAttributes.instance.transform);
        takingBuyer = false;
    }
}
