﻿using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using UnityEngine;
using UnityEngine.UI;

public class StoragePanelManageMent : MonoBehaviour
{
    public enum StorageState
    {
        idle , itemAdded , itemQuery , polishing
    }

    public StorageState storageState;
    public Camera mainCam;
    public Camera storageCam;
    public Camera polishCam;

    public GameObject GamePanel;
    public GameObject StoragePanel;
    public GameObject polishPanel;
    public GameObject ButtonQuickSell;

    public Image WhiteCover;

    public Color WhiteTrans;
    public Color WhiteSolid;

    public float transitionSpeed;

    public bool StorageOn;
    public bool transition;
    bool transitionOut;

    float whiteValue;

    public Text TitleText;
    public Text BuyValueText;
    public Text SellValueText;
    public Text ProfitText;

    public int SellingOfferValue;

    public GameObject infoBoard;
    public GameObject nothingSelectedBoard;
    public GameObject storageFullBoard;
    public GameObject polishButton, bonusButton;

    public GameObject QueryBoard;
    public int queryIndex;
    public Text queryHeaderText;
    public Text queryHeaderTextUp;
    public Text sellValueUp;
    public Text conditionText;
    public Image queryItemPP;
    public Image queryCharPP;

    public static StoragePanelManageMent instance;

    public int selectedItemRootIndex;
    public int selectedSlotIndex;
    public int selecteditemUniqueIndex;



    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        StoragePanel.SetActive(false);

        mainCam.gameObject.SetActive(true);
        storageCam.gameObject.SetActive(false);
        polishCam.gameObject.SetActive(false);

        CloseQueryPanel();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (transition)
        {
            whiteValue = Mathf.MoveTowards(whiteValue, 1, Time.deltaTime * transitionSpeed);
            WhiteCover.color = Color.Lerp(WhiteTrans, WhiteSolid, whiteValue);

            if (whiteValue == 1)
            {
                transition = false;

                if (!StorageOn)
                {
                    StorageOn = true;

                    transitionOut = true;
                    GoToStorage();
                }
                else
                {
                    StorageOn = false;

                    transitionOut = true;
                    LeaveStorage();
                }

            }

        }
        else if(transitionOut)
        {
            whiteValue = Mathf.MoveTowards(whiteValue, 0, Time.deltaTime * transitionSpeed);
            WhiteCover.color = Color.Lerp(WhiteTrans, WhiteSolid, whiteValue);

            if (whiteValue == 0)
            {
                transitionOut = false;

                //if (!StorageOn)
                //{
                //    StorageOn = true;
                //    //GoToStorage();
                //}
                //else
                //{
                //    StorageOn = false;
                //    //LeaveStorage();
                //}

            }

        }

    }

    public void GoToStorage()
    {
        UiManage.instance.CloseAllPanels();
        GamePanel.SetActive(false);
        StoragePanel.SetActive(true);

        mainCam.gameObject.SetActive(false);
        storageCam.gameObject.SetActive(true);
        polishCam.gameObject.SetActive(false);
    }

    public void LeaveStorage()
    {
        UiManage.instance.GetRightPanel((int)GameManagement.instance.state);
        GamePanel.SetActive(true);
        StoragePanel.SetActive(false);

        mainCam.gameObject.SetActive(true);
        storageCam.gameObject.SetActive(false);
        polishCam.gameObject.SetActive(false);

        CloseQueryPanel();
    }

    public void TransitionOut()
    {
        transition = true;
        if(storageState == StorageState.itemAdded)
        {
            GameStatesControl.instance.NextSellerButton();
        }
        if (StorageOn)
        {
            storageState = StorageState.idle;
        }
        CloseQueryPanel();
        StorageManager.instance.ClearQueryList();
        
    }
    
    public void TransitionIn()
    {
        transition = true;
        

        HideInfoBoard();
        StorageManager.instance.DisableAllHighlights();

        if (storageState == StorageState.itemAdded)
        {
            StorageManager.instance.AddAnItemToStorage(ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemId);
        }
        else if (storageState == StorageState.itemQuery)
        {
            StorageManager.instance.QueryCheck();
            GetQueryPanel();
        }

    }

    public void StoragePanelInfoUpdate(int i)
    {
        
        TitleText.text = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>(). title;
        BuyValueText.text = "$ "+ StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().buyValue;

        conditionText.text = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().itemCond.ToString();
        SellingOfferValue = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().EstValue;
        selectedItemRootIndex = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().itemId;
        selecteditemUniqueIndex = StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().itemUniqueIdInShelf;

        if (storageState == StorageState.itemQuery)
        {
            if(GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().buyingItemCondition == ItemToSell.ItemCondition.Excellent)
            {
                //sellValueUp.text = "$ " +(int)(SellingOfferValue * 1.2f);
                sellValueUp.text = "$ " + GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().BuyValueHud;

            }
            else
            {
                //sellValueUp.text = "$ " + (int)SellingOfferValue;
                sellValueUp.text = "$ " + GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().BuyValueHud;
            }
            SellValueText.text = "$ " + (int)SellingOfferValue;
            //sellValueUp.text = "$ " + SellingOfferValue;
            ProfitText.text = "$ " + (SellingOfferValue - StorageManager.instance.itemsOnShelf[i].GetComponent<ItemToSell>().buyValue);
        }
        else
        {
            SellValueText.text = "$ " + SellingOfferValue;
            ProfitText.text = " N / A ";
        }

        GetInfoBoard();


    }

    public void GetInfoBoard()
    {
        infoBoard.SetActive(true);
        nothingSelectedBoard.SetActive(false);
    }

    public void HideInfoBoard()
    {
        infoBoard.SetActive(false);
        nothingSelectedBoard.SetActive(true);
    }

    public void QuickSellButton()
    {
        GameStatesControl.instance.GetSellingState();

        transition = true;
        
        if (StorageOn)
        {
            storageState = StorageState.idle;
        }
        StorageManager.instance.itemSlots[selectedSlotIndex].RemoveItemStart();
        CloseQueryPanel();
        StorageManager.instance.ClearQueryList();
    }

    public void SellToBuyerButton()
    {
        //GameStatesControl.instance.GetSellingState();

        transition = true;

        if (StorageOn)
        {
            storageState = StorageState.idle;
        }
        StorageManager.instance.itemSlots[selectedSlotIndex].RemoveItemStart();
        //CloseQueryPanel();
        StorageManager.instance.ClearQueryList();

        GameStatesControl.instance. SellItemToBuyer();
    }

    public void GetQueryPanel()
    {
        QueryBoard.SetActive(true);
        Debug.Log(StorageManager.instance.QueryList.Count);
        if(StorageManager.instance.QueryList.Count > 0)
        {
            StorageManager.instance.QueryList[0].ButtonCheck();
        }
        

        QueryBoardInfoUpdate();
    }

    public void CloseQueryPanel()
    {
        QueryBoard.SetActive(false);
    }

    public void NextQuery()
    {
        if (StorageManager.instance.QueryList.Count > 0)
        {
            if (queryIndex < StorageManager.instance.QueryList.Count - 1)
            {
                queryIndex += 1;
            }
            else
            {
                queryIndex = 0;
            }
            if (queryIndex >= 0)
            {
                StorageManager.instance.QueryList[queryIndex].ButtonCheck();
            }

            QueryBoardInfoUpdate();
        }

        

    }
    public void PrevQuery()
    {
        if (StorageManager.instance.QueryList.Count > 0)
        {
            if (queryIndex > 0)
            {
                queryIndex -= 1;
            }
            else
            {
                queryIndex = StorageManager.instance.QueryList.Count - 1;
            }
            if (queryIndex >= 0)
            {
                StorageManager.instance.QueryList[queryIndex].ButtonCheck();
            }
            QueryBoardInfoUpdate();
        }

        
    }
    public void QueryBoardInfoUpdate()
    {
        if (StorageManager.instance.QueryList.Count > 0)
        {
            queryHeaderText.text = StorageManager.instance.queryItemType.ToString() + " " + (queryIndex + 1) + " / " + StorageManager.instance.QueryList.Count;
        }
        else
        {
            queryHeaderText.text = StorageManager.instance.queryItemType.ToString() + " " + 0 + " / " + StorageManager.instance.QueryList.Count;
        }
        if (GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().buyingItemCondition == ItemToSell.ItemCondition.Excellent)
        {
            queryHeaderTextUp.text = StorageManager.instance.queryItemCondition + " " + StorageManager.instance.queryItemType.ToString();

        }
        else
        {
            queryHeaderTextUp.text =  StorageManager.instance.queryItemType.ToString();
        }

        //queryItemPP.sprite = StorageManager.instance.itemsCreated[(int)StorageManager.instance.queryItemType].itemImage;
        queryItemPP.sprite = GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().buyingItemImage.sprite;
        queryCharPP.sprite = StorageManager.instance.itemData.charPPs[GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().charInsideIndex];
    }

    public void StorageFullBoardAppear()
    {
        storageFullBoard.SetActive(true);
    }
    public void StorageFullBoardDisAppear()
    {
        storageFullBoard.SetActive(false);
    }

    public void PolishPanelApper()
    {
        mainCam.gameObject.SetActive(false);
        storageCam.gameObject.SetActive(false);
        polishCam.gameObject.SetActive(true);

        polishPanel.SetActive(true);
        StoragePanel.SetActive(false);

        PolishSyestem.instance.PolishInitalize();
    }

    public void PolishPanelLeave()
    {
        mainCam.gameObject.SetActive(false);
        storageCam.gameObject.SetActive(true);
        polishCam.gameObject.SetActive(false);

        polishPanel.SetActive(false);
        StoragePanel.SetActive(true);

        PolishSyestem.instance.sparkleParticle.Stop();
        PolishSyestem.instance.sparkleParticle.Clear();

        StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].
            GetComponent<ItemToSell>().polishedOrNonPolishedMaterial();
        StorageManager.instance.itemsConditionIds[StoragePanelManageMent.instance.selectedSlotIndex]
            =(int) StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().itemCond;
        StorageManager.instance.ConditionsSaveAfterPolish();
        StorageManager.instance.itemSlots[StoragePanelManageMent.instance.selectedSlotIndex].
            ButtonCheck();

        ButtonQuickSell.GetComponentInChildren<Animator>().SetTrigger("add20");
    }

    public void PolishButtonOrNot()
    {
        if(StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().itemCond 
            == ItemToSell.ItemCondition.Excellent)
        {
            polishButton.SetActive(false);
            bonusButton.SetActive(!BonusIsTaken());

        }
        else
        {
            polishButton.SetActive(true);
            bonusButton.SetActive(false);
        }
    }

    public bool BonusIsTaken()
    {
        string name = StorageManager.instance.itemsOnShelf[instance.selectedSlotIndex].GetComponent<ItemToSell>().name;
        return PlayerPrefsX.GetBool(name);
    }

    public void TakeBonus()
    {
        //todo polish
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("polish");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                StorageManager.instance.itemsOnShelf[instance.selectedSlotIndex].GetComponent<ItemToSell>().EstValue *= 2;
                string name = StorageManager.instance.itemsOnShelf[instance.selectedSlotIndex].GetComponent<ItemToSell>().name;
                PlayerPrefsX.SetBool(name, true);
                PolishButtonOrNot();
                PolishPanelLeave();
            }
        },false,false);
    }
}
