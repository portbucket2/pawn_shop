﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellerCharactersMamanger : MonoBehaviour
{
    public GameObject[] sellerChars;
    public int currentSellerIndex;
    public static SellerCharactersMamanger instance;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        currentSellerIndex = GameManagement.instance.levelIndex % sellerChars.Length;
        for (int i = 0; i < sellerChars.Length; i++)
        {
            if (sellerChars[i].GetComponent<CharacterAnimController>().Thief)
            {
                WantedBorad.instance.wantedPpSprites.Add(StorageManager.instance.itemData.charPPs[sellerChars[i].GetComponent<CharacterAnimController>().charInsideIndex]);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetASeller()
    {
        
        
        GameStatesControl.instance.currentCharacter = sellerChars[currentSellerIndex];
        GameStatesControl.instance.BuyerMode = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().Buyer;
        GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().ThiefStrapEnableOrNot();
        GameStatesControl.instance.customerType = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().customerType;
        GameStatesControl.instance.robbingAmount = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().robbingAmount;
        GameStatesControl.instance.vipEstimatedAmount = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().vipEstimatedAmount;

        if (GameStatesControl.instance.customerType.Equals(CustomerType.ROBBER))
        {
            sellerChars[currentSellerIndex].GetComponent<CharacterAnimController>().item.SetActive(false);

        }
        else if (GameStatesControl.instance.BuyerMode)
        {
            StorageManager.instance.queryItemType = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().buyingItem;
            StorageManager.instance.queryItemCondition = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().buyingItemCondition;
            sellerChars[currentSellerIndex].GetComponent<CharacterAnimController>().item.SetActive(false);
            GameStatesControl.instance.Hasthief = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().Thief;
            DollarBills.instance.DollarIntoHandInstant();
        }
        else
        {
            sellerChars[currentSellerIndex].GetComponent<CharacterAnimController>().item.SetActive(true);
            ScannerAttributes.instance.currentItem = sellerChars[currentSellerIndex].GetComponent<CharacterAnimController>().item;
            AccountsControl.instance.GetDataOfNewItem(ScannerAttributes.instance.currentItem);
            ScreenAttributes.instance.UpdateMonitorInfo(ScannerAttributes.instance.currentItem);
            GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().SellValue = ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().buyValue;

            //SellingPanel.instance.LoadItemImage();

            GameStatesControl.instance.Hasthief = GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().Thief;
            DollarBills.instance.DollarBackToRootInstant();
        }

        // If this is VIP then replace estimatated value
        // and offer 10% of estimatated value as a asking value
        if (GameStatesControl.instance.customerType.Equals(CustomerType.VIP))
        {
            // First update new price in all current references
            ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().EstValue =
                GameStatesControl.instance.currentCharacter.GetComponent<CharacterAnimController>().vipEstimatedAmount;
            ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().buyValue =
                ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().EstValue * 10 / 100;

            StorageManager.instance.itemsCreated[ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemId].buyValue =
                ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().buyValue;
            StorageManager.instance.itemsCreated[ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemId].EstValue =
                ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().EstValue;


            GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().SellValue =
                ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().buyValue;

            AccountsControl.instance.GetDataOfNewItem(ScannerAttributes.instance.currentItem);
            ScreenAttributes.instance.UpdateMonitorInfo(ScannerAttributes.instance.currentItem);

            GameStatesControl.instance.currentCharacter.GetComponent<charHUD>().discountRV.SetActive(false);
        }


        for (int i = 0; i < sellerChars.Length; i++)
        {
            if(i == currentSellerIndex)
            {
                sellerChars[currentSellerIndex].SetActive(true);
                sellerChars[currentSellerIndex].GetComponent<CharacterAnimController>().IdentifyBuyerCharAnim();
            }
            else
            {
                if (sellerChars[i])
                {
                    sellerChars[i].GetComponent<CharPathFollowing>().GetBackCharToRoot();
                    sellerChars[i].SetActive(false);
                }
            }
            
            //sellerChars[currentSellerIndex].GetComponent<CharacterAnimController>().rejected = false;
        }
    }
    public void GetNextSeller()
    {
        //GetNextSellerIndex();
        GetASeller();
        WantedBorad.instance.BustedAnimReset();

        if (currentSellerIndex == 0)
        {
            //WantedBorad.instance.BustedAnimReset();
        }
    }
    public void GetNextSellerIndex()
    {
        currentSellerIndex += 1;
        if (currentSellerIndex >= sellerChars.Length)
        {
            currentSellerIndex = 0;
        }
        GameManagement.instance.LevelAccomplished();
        GameManagement.instance.NextLevel();
    }
}
