﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenAttributes : MonoBehaviour
{
    public static ScreenAttributes instance;
    public GameObject[] monitorPanels;

    public Text TitleText;
    public Text TypeText;
    public Text conditionText;
    public Text EstimatedValueText;

    public GameObject thiefBoard;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AppearPanelIdle()
    {
        AppearPanelIndex(0);
    }
    public void AppearPanelScanning()
    {
        AppearPanelIndex(1);
    }
    public void AppearPanelInfo()
    {
        AppearPanelIndex(2);
    }
    void AppearPanelIndex( int p)
    {
        for (int i = 0; i < monitorPanels.Length; i++)
        {
            if (i == p)
            {
                monitorPanels[i].SetActive(true);
            }
            else
            {
                monitorPanels[i].SetActive(false);
            }
        }

        if (GameStatesControl.instance.Hasthief)
        {
            thiefBoard.SetActive(true);
        }
        else
        {
            thiefBoard.SetActive(false);
        }
    }

    public void UpdateMonitorInfo( GameObject item)
    {
        TitleText.text = item.GetComponent<ItemToSell>().title;
        TypeText.text = item.GetComponent<ItemToSell>().type;
        //conditionText.text = item.GetComponent<ItemToSell>().condition;
        conditionText.text = item.GetComponent<ItemToSell>().itemCond.ToString();
        EstimatedValueText.text ="$ "+ item.GetComponent<ItemToSell>().EstValue ;
        
    }
}
