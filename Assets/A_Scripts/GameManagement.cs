﻿using TMPro;
using UnityEngine.UI;
using UnityEngine;
using Portbliss.Ad;
using System;

public class GameManagement : MonoBehaviour
{
    
    public enum State
    {
        idle, walkToDesk, reachedDesk, scanning , Decision , Bidding , result , selling, soldResult
    }

    public int levelIndex;
    public int dollarsPP;
    public int thiefPPref;

    public State state;

    public static GameManagement instance;

    public ParticleSystem particleDollars;
    public ParticleSystem particleSold;

    [Header("Day & Level Count properties")]
    public GameObject dayInfo;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI dayText;
    public Slider levelSlider;
    public int startingLevel = 35;


    bool firstTimeLoad;
    int levelsInADay = 5;

    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        //PlayerPrefs.SetInt("levelIndex", startingLevel - 1);
        instance = this;

        levelIndex = PlayerPrefs.GetInt("levelIndex", 0);
        dollarsPP = PlayerPrefs.GetInt("dollarsPP", 1000);
        thiefPPref = PlayerPrefs.GetInt("thiefPPref", 0);

        firstTimeLoad = true;
        dayInfo.SetActive(false);
    }


    public void ChangeStateTo(int s)
    {
        state = (State)s;
        UiManage.instance.GetRightPanel(s);
    }

    public void LevelStarted()
    {
        
        Debug.Log("Level Started" + (levelIndex + 1));
        AnalyticsControllerOriginal.LogEvent_LevelStarted((levelIndex + 1));

        UiManage.instance.UpdateAllValueUi();

        if (!firstTimeLoad)
        {
            GameUtil.LogGreen("will try to show interstitial ad, sent call to wrapper");
            AdvertisementWrapper.AdIteration((success) =>
            {
                GameUtil.LogGreen("interstitial ad has been shown? " + success);
                //put callback.
            }, false, true);
        }
        firstTimeLoad = false;
        UpdateDayCountUI();
    }

    private void UpdateDayCountUI()
    {
        dayInfo.SetActive(true);
        dayText.text = "Day " + (levelIndex / levelsInADay + 1);
        levelText.text = "Lv-" + (levelIndex + 1);
        levelSlider.value = Mathf.InverseLerp(0, levelsInADay, (levelIndex + 1) % levelsInADay == 0 ? levelsInADay : (levelIndex + 1) % levelsInADay);
    }

    public bool EndOfTheDay()
    {
        return (levelIndex % levelsInADay) == 0;
    }

    public void LevelAccomplished()
    {
        Debug.Log("Level Accomplished" + (levelIndex + 1));
        AnalyticsControllerOriginal.LogEvent_LevelCompleted((levelIndex + 1));
        UpdateDayCountUI();
    }

    public void NextLevel()
    {
        levelIndex += 1;

        PlayerPrefs.SetInt("levelIndex", levelIndex);
    }

    public void DollarsPPSave()
    {
        dollarsPP = AccountsControl.instance.currentBalance;
        PlayerPrefs.SetInt("dollarsPP", dollarsPP);
    }
}
