﻿using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using UnityEngine;
using UnityEngine.UI;

public class charHUD : MonoBehaviour
{
    public GameObject HUD;
    public GameObject speechBubble, discountRV;
    public GameObject ratingBoard;
    public int SellValue;
    public int BuyValueHud;
    public Text SellValueText;
    public Text BuyValueText;
    public Text TextHeader;
    public GameObject BuyerBoard;
    public GameObject SellerBoard;
    public GameObject RobberBoard;
    public Text robberDemandText;
    public GameObject VIPBoard;
    public Image buyingItemImage;
    public Sprite[] imojis;
    public Image[] starFills;
    public Image imojiProjected;
    public int rating;
    public Text expressionText;
    public string[] expressionsRated;
    // Start is called before the first frame update
    void Start()
    {
        CloseHud();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AppearSpeechBubble()
    {
        
        HUD.SetActive(true);
        //speechBubble.SetActive(true);
        ratingBoard.SetActive(false);
                

        if (GetComponent<CharacterAnimController>().Buyer)
        {

            BuyerBoard.gameObject.SetActive(true);
            SellerBoard.gameObject.SetActive(false);
            int i = (int)GetComponentInParent<CharacterAnimController>().buyingItem;
            if (GetComponent<CharacterAnimController>().buyingItemCondition == ItemToSell.ItemCondition.Excellent)
            {
                TextHeader.text = ""+ GetComponent<CharacterAnimController>().buyingItemCondition + " " + GetComponent<CharacterAnimController>().buyingItem;
                BuyValueHud = (int)(GetComponent<CharacterAnimController>().item.GetComponent<ItemToSell>().EstValue * 1.2f);
                BuyValueText.text = "" + BuyValueHud;
            }
            else
            {
                TextHeader.text = "" + GetComponent<CharacterAnimController>().buyingItem;

                BuyValueHud = (int)GetComponent<CharacterAnimController>().item.GetComponent<ItemToSell>().EstValue;
                BuyValueText.text = "" + BuyValueHud;
            }

            //buyingItemImage.sprite = StorageManager.instance.itemsCreated[(int)GetComponent<CharacterAnimController>().buyingItem].itemImage;
            buyingItemImage.sprite = 
                GetComponent<CharacterAnimController>().item.GetComponent<ItemToSell>().itemImagesAccToType[(int)GetComponent<CharacterAnimController>().buyingItem];


        }
        else
        {
            //if (GetComponent<CharacterAnimController>().customerType.Equals(CustomerType.ROBBER))
            //{
            //    BuyerBoard.gameObject.SetActive(false);
            //    SellerBoard.gameObject.SetActive(false);
            //    RobberBoard.gameObject.SetActive(true);
            //    VIPBoard.gameObject.SetActive(false);

            //    robberDemandText.text = "Give me $" + GetComponent<CharacterAnimController>().robbingAmount + "!!!";
            //}
            //else if (GetComponent<CharacterAnimController>().customerType.Equals(CustomerType.VIP))
            //{
            //    BuyerBoard.gameObject.SetActive(false);
            //    SellerBoard.gameObject.SetActive(false);
            //    RobberBoard.gameObject.SetActive(false);
            //    VIPBoard.gameObject.SetActive(true);
            //}
            //else if (GetComponent<CharacterAnimController>().customerType.Equals(CustomerType.ORDINARY))
            //{
            //    BuyerBoard.gameObject.SetActive(true);
            //    SellerBoard.gameObject.SetActive(false);
            //    RobberBoard.gameObject.SetActive(false);
            //    VIPBoard.gameObject.SetActive(false);
            //}

            BuyerBoard.gameObject.SetActive(false);
            SellerBoard.gameObject.SetActive(true);
            SellValueText.text = "" + SellValue;
        }
    }

    public void Get_50_Percent_Off(GameObject button)
    {
        //todo discount
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("discount");
        AdvertisementWrapper.AdIteration((success) =>
        {
            GameUtil.LogGreen("interstitial ad has been shown? " + success);
            //put callback.
            if (success)
            {
                SellValue /= 2;
                SellValueText.text = "" + SellValue;

                ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().buyValue = SellValue;
                AccountsControl.instance.GetDataOfNewItem(ScannerAttributes.instance.currentItem);
                StorageManager.instance.itemsCreated[ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemId].buyValue = SellValue;
                //StorageManager.instance.itemsOnShelf[].GetComponent<ItemToSell>().buyValue = SellValue;

                button.SetActive(false);
            }

        }, false, false);
    }

    public void EnabeSpeachBubbleFor()
    {
        HUD.SetActive(true);
        if (GetComponent<CharacterAnimController>().customerType.Equals(CustomerType.ROBBER))
        {
            BuyerBoard.gameObject.SetActive(false);
            SellerBoard.gameObject.SetActive(false);
            RobberBoard.gameObject.SetActive(true);
            VIPBoard.gameObject.SetActive(false);

            robberDemandText.text = "Give me $" + GetComponent<CharacterAnimController>().robbingAmount + "!!!";
        }
        else if (GetComponent<CharacterAnimController>().customerType.Equals(CustomerType.VIP))
        {
            BuyerBoard.gameObject.SetActive(false);
            SellerBoard.gameObject.SetActive(false);
            RobberBoard.gameObject.SetActive(false);
            VIPBoard.gameObject.SetActive(true);
        }
        else if (GetComponent<CharacterAnimController>().customerType.Equals(CustomerType.ORDINARY))
        {
            BuyerBoard.gameObject.SetActive(true);
            SellerBoard.gameObject.SetActive(false);
            RobberBoard.gameObject.SetActive(false);
            VIPBoard.gameObject.SetActive(false);
        }
    }

    public void CloseHud()
    {
        HUD.SetActive(false);
        BuyerBoard.gameObject.SetActive(false);
        SellerBoard.gameObject.SetActive(false);
        RobberBoard.gameObject.SetActive(false);
        VIPBoard.gameObject.SetActive(false);
    }
    public void RatingHudEnable()
    {
        
        HUD.SetActive(true);
        
        ratingBoard.SetActive(true);
        BuyerBoard.gameObject.SetActive(false);
        SellerBoard.gameObject.SetActive(false);
        //speechBubble.SetActive(false);

        if (GetComponent<CharacterAnimController>().Buyer &&(GetComponent<CharacterAnimController>().buyingItemCondition == ItemToSell.ItemCondition.Excellent))
        {
            if (StorageManager.instance.itemsOnShelf[StoragePanelManageMent.instance.selectedSlotIndex].GetComponent<ItemToSell>().itemCond
                == ItemToSell.ItemCondition.Excellent)
            {
                Rateit(4);
            }
            else
            {
                Rateit(2);
            }
        }
        
        else
        {
            Rateit(4);
        }
        
        Invoke("CloseHud", 2f);
    }
    public void RatingHudReject()
    {

        HUD.SetActive(true);

        ratingBoard.SetActive(true);
        BuyerBoard.gameObject.SetActive(false);
        SellerBoard.gameObject.SetActive(false);
        //speechBubble.SetActive(false);
        Rateit(0);
        Invoke("CloseHud", 2f);
    }
    void Rateit(int rate)
    {
        imojiProjected.sprite = imojis[rate ];
        expressionText.text = expressionsRated[rate ];
        for (int i = 0; i < imojis.Length; i++)
        {
            if (i <= rate)
            {
                starFills[i].gameObject.SetActive(true);
            }
            else
            {
                starFills[i].gameObject.SetActive(false);
            }
        }

        MasterRatingUiUp.instance.MasterRateFromSingleRating(rate + 1);
    }

    

}
