﻿using System.Collections;
using System.Collections.Generic;
using Portbliss.Ad;
using UnityEngine;

public class StorageManager : MonoBehaviour
{
    public int ShelfCount;
    
    public GameObject ShelfPrefab;
    public List<GameObject> Shelves;
    
    public List<ItemStorageHolder> itemSlots;
    public List<int> itemsIds;
    public List<int> itemsConditionIds;
    public List<GameObject> itemsOnShelf;

    public GameObject shelfHolder;
    public GameObject itemsHolder;

    public float shelfGap;
    public int itemSlotsPerShelf;
    //public int itemSlotsCapacity;

    public ItemToSell[] itemsCreated;

    public static StorageManager instance;

    public ItemToSell.ItemTypes queryItemType;
    public ItemToSell.ItemCondition queryItemCondition;

    public List<ItemStorageHolder> QueryList;

    public int lastUniqueId;

    public ScriptableObjCollectionsData itemData;

    public string itemIdsSaved;
    public string[] split;
    public List<int> savedIds;

    public string itemCondsSaved;
    public string[] splitConds;
    public List<int> savedConds;

    private void Awake()
    {
        instance = this;
        GetItemIdsFromSavedData();
        

        itemData.itemPPs = new Sprite[itemData.itemPrefabs.Length];
        itemData.nonPolishedMats = new Material[itemData.itemPrefabs.Length];
        itemData.polishedMats = new Material[itemData.itemPrefabs.Length];
        for (int i = 0; i < itemData.itemPPs.Length; i++)
        {
            itemData.itemPPs[i] = itemData.itemPrefabs[i].GetComponent<ItemToSell>().itemImage;
            itemData.nonPolishedMats[i] = itemData.itemPrefabs[i].GetComponent<ItemToSell>().nonPolishedMat;
            itemData.polishedMats[i] = itemData.itemPrefabs[i].GetComponent<ItemToSell>().polishedMat;
        }
        GetItemData();


    }
    // Start is called before the first frame update
    void Start()
    {

        ShelfsInitialization();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G))
        {
            AddAShelf();
            AddAnItemToStorage(ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemId);
        }

    }
    public void GetItemData()
    {
        itemsCreated = new ItemToSell[itemData.itemPrefabs.Length];
        for (int i = 0; i < itemsCreated.Length; i++)
        {
            itemsCreated[i] = itemData.itemPrefabs[i].GetComponent<ItemToSell>();
            itemData.nonPolishedMats[i].SetTexture("_MainTex", itemData.texNonPolished[i]);
            itemData.polishedMats[i].SetTexture("_MainTex", itemData.texPolished[i]);
            //itemsCreated[i].itemImage = itemData.itemPPs[i];
            //itemsCreated[i].itemId =  i;
        }
    }
    public void AddAnItemToStorage(int itemId)
    {
        if(itemsOnShelf.Count < itemSlots.Count)
        {
            itemsIds.Add(itemId);
            GameObject go = Instantiate(itemsCreated[itemId].gameObject, itemsHolder.transform.position, itemsHolder.transform.rotation);
            go.transform.SetParent(itemsHolder.transform);
            itemsOnShelf.Add(go);

            lastUniqueId += 1;
            go.GetComponent<ItemToSell>().itemUniqueIdInShelf = lastUniqueId;
            
            

            for (int i = 0; i < itemSlots.Count; i++)
            {
                itemSlots[i].GetComponent<ItemStorageHolder>().GetItemInfo(i);
            }

            itemSlots[itemsOnShelf.Count - 1].GetComponent<ItemStorageHolder>().GetItemFromOutScreen();


            //StorageShelfSliding.instance.ShelfSnapByItemSlot(itemsOnShelf.Count);
            itemSlots[itemsOnShelf.Count - 1].GetComponent<ItemStorageHolder>().ButtonCheck();

            AddedItemIdsToSavedData(itemId);
        }
        else
        {
            StoragePanelManageMent.instance.StorageFullBoardAppear();
        }
        
    }

    public void RemoveFromStorage(int index)
    {
        itemsIds.Remove(itemsIds[index]);

        Destroy(itemsOnShelf[index]);
        itemsOnShelf.Remove(itemsOnShelf[index]);
        //GameObject go = Instantiate(itemsCreated[itemsIds[itemId]].gameObject, itemsHolder.transform.position, itemsHolder.transform.rotation);
        //go.transform.SetParent(itemsHolder.transform);
        //itemsOnShelf.Add(go);
        //
        for (int i = 0; i < itemSlots.Count; i++)
        {
            itemSlots[i].GetComponent<ItemStorageHolder>().GetItemInfo(i);
        }
        RemovedItemIdsToSavedData();
    }

    public void HighlightSelected(int i)
    {
        DisableAllHighlights();
        itemSlots[i].HighlightIt();
        StoragePanelManageMent.instance.selectedSlotIndex = i;
    }
    public void DisableAllHighlights()
    {
        for (int i = 0; i < itemSlots.Count; i++)
        {
            itemSlots[i].DisableHighlight();
        }
    }

    public void QueryCheck()
    {
        ItemToSell.ItemTypes queryItem = queryItemType;

        int queryIndexx = 0;
        for (int i = 0; i < itemSlots.Count; i++)
        {
            if (itemSlots[i].itemObj)
            {
                if (itemSlots[i].itemObj.GetComponent<ItemToSell>().itemType == queryItem)
                {
                    itemSlots[i].itemObj.GetComponent<ItemToSell>().MakeItemNonTransparent();
                    itemSlots[i].queryIndex = queryIndexx;
                    queryIndexx += 1;

                    QueryList.Add(itemSlots[i]);
                }
                else
                {
                    itemSlots[i].itemObj.GetComponent<ItemToSell>().MakeItemTransparent();
                }
            }
            
        }
    }

    public void ClearQueryList()
    {
        QueryList.Clear();
        for (int i = 0; i < itemSlots.Count; i++)
        {
            itemSlots[i].queryIndex = -1;
            if (itemSlots[i].itemObj)
            {
                itemSlots[i].itemObj.GetComponent<ItemToSell>().MakeItemNonTransparent();
            }
            
        }
    }

    public void AddAShelf()
    {
        ShelfCount += 1;
        
        GameObject go = Instantiate(ShelfPrefab, shelfHolder.transform.position, shelfHolder.transform.rotation);
        go.transform.SetParent(shelfHolder.transform);
        go.transform.localPosition = new Vector3(shelfGap * (ShelfCount -1), 0, 0);
        for (int j = 0; j < go.GetComponent<ShelfDisplay>().itemSlots.Length; j++)
        {
            itemSlots.Add(go.GetComponent<ShelfDisplay>().itemSlots[j]);
            go.GetComponent<ShelfDisplay>().itemSlots[j].ItemStorageHolderIndex = itemSlots.Count - 1;
        }

        Shelves.Add(go);
        for (int i = 0; i < itemSlots.Count; i++)
        {
            itemSlots[i].GetComponent<ItemStorageHolder>().GetItemInfo(i);
        }

        StorageShelfSliding.instance.numBerOfShelves = GetComponent<StorageManager>().ShelfCount;

        AddAnItemToStorage(ScannerAttributes.instance.currentItem.GetComponent<ItemToSell>().itemId);

        StoragePanelManageMent.instance.StorageFullBoardDisAppear();

    }

    public void AddAShelfByMoney(int value)
    {
        AccountsControl.instance.currentBoughtvalue = value;
        if (AccountsControl.instance.SpendBoughtValue())
        {
            AddAShelf();
        }
    }

    public void AddAShelfByVideo()
    {
        //todo shelf
        AnalyticsControllerOriginal.SetCurrentPlacementForRV_Analytics("shelf");
        AdvertisementWrapper.AdIteration((success) =>
        {
            if (success)
            {
                AddAShelf();
            }
            
        },false,false);
    }
    public void ShelfsInitialization()
    {
        ShelfCount = (itemsIds.Count / itemSlotsPerShelf) + 1;
        for (int i = 0; i < ShelfCount; i++)
        {
            GameObject go = Instantiate(ShelfPrefab, shelfHolder.transform.position, shelfHolder.transform.rotation);
            go.transform.SetParent(shelfHolder.transform);
            go.transform.localPosition = new Vector3(shelfGap * i, 0, 0);

            for (int j = 0; j < go.GetComponent<ShelfDisplay>().itemSlots.Length; j++)
            {
                itemSlots.Add(go.GetComponent<ShelfDisplay>().itemSlots[j]);
                go.GetComponent<ShelfDisplay>().itemSlots[j].ItemStorageHolderIndex = itemSlots.Count - 1;
            }

            Shelves.Add(go);
        }

        for (int i = 0; i < itemsIds.Count; i++)
        {
            GameObject go = Instantiate(itemsCreated[itemsIds[i]].gameObject, itemsHolder.transform.position, itemsHolder.transform.rotation);
            go.transform.SetParent(itemsHolder.transform);
            go.GetComponent<ItemToSell>().itemUniqueIdInShelf = i;
            go.GetComponent<ItemToSell>().itemCond = (ItemToSell.ItemCondition)itemsConditionIds[i];
            lastUniqueId = i;
            itemsOnShelf.Add(go);
        }

        for (int i = 0; i < itemSlots.Count; i++)
        {
            itemSlots[i].GetComponent<ItemStorageHolder>().GetItemInfo(i);
        }
        StorageShelfSliding.instance.numBerOfShelves = GetComponent<StorageManager>().ShelfCount;
        
    }

    public void GetItemIdsFromSavedData()
    {
        
        itemIdsSaved = PlayerPrefs.GetString("itemIdsSaved", "-1");
        itemCondsSaved = PlayerPrefs.GetString("itemCondsSaved", "-1");
        //itemIdsSaved = "0_5_3_2_2";
        //PlayerPrefs.SetString("itemIdsSaved", itemIdsSaved);
        //itemIdsSaved = PlayerPrefs.GetString("itemIdsSaved", "0");
        split = itemIdsSaved.Split(char.Parse("_"));
        splitConds = itemCondsSaved.Split(char.Parse("_"));
        for (int i = 0; i < split.Length; i++)
        {
            int val;
            int.TryParse(split[i], out val);
            savedIds.Add(val);
            if (val >= 0)
            {
                itemsIds.Add(val);
            }
            int.TryParse(splitConds[i], out val);
            savedConds.Add(val);
            if (val >= 0)
            {
                itemsConditionIds.Add(val);
            }
        }

    }

    public void AddedItemIdsToSavedData( int itemId)
    {
        itemIdsSaved += ("_" + itemId);
        savedIds.Add(itemId);
        int condIndex =(int) itemsOnShelf[itemsOnShelf.Count - 1].GetComponent<ItemToSell>().itemCond;
        itemCondsSaved += ("_" + condIndex);
        savedConds.Add(condIndex);
        itemsConditionIds.Add(condIndex);
        PlayerPrefs.SetString("itemIdsSaved", itemIdsSaved);
        PlayerPrefs.SetString("itemCondsSaved", itemCondsSaved);
    }
    public void RemovedItemIdsToSavedData()
    {
        savedIds.Clear();
        savedIds.Add(-1);

        savedConds.Clear();
        savedConds.Add(-1);

        itemIdsSaved = "" + (-1);
        itemCondsSaved = "" + (-1);
        for (int i = 0; i < itemsIds.Count; i++)
        {
            savedIds.Add(itemsIds[i]);
            savedConds.Add(itemsConditionIds[i]);
            itemIdsSaved += ("_" + itemsIds[i]);
            itemCondsSaved += ("_" + itemsConditionIds[i]);
        }
        PlayerPrefs.SetString("itemIdsSaved", itemIdsSaved);
        PlayerPrefs.SetString("itemCondsSaved", itemCondsSaved);
    }
    public void ConditionsSaveAfterPolish()
    {
        savedConds.Clear();
        savedConds.Add(-1);
        
        itemCondsSaved = "" + (-1);
        for (int i = 0; i < itemsIds.Count; i++)
        {
            savedConds.Add(itemsConditionIds[i]);
            itemCondsSaved += ("_" + itemsConditionIds[i]);
        }
        PlayerPrefs.SetString("itemCondsSaved", itemCondsSaved);
    }


}
