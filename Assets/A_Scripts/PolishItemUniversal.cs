﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolishItemUniversal : MonoBehaviour
{
    //public Material nonPolishedMat;
    public Material polishMat;

    public GameObject[] ItemsToPolish;

    public Texture2D[] texNonPolished;

    public int polishItemIndex;
    public ScriptableObjCollectionsData itemData;

    // Start is called before the first frame update
    void Start()
    {
        //PolishItemChoose(3);
        //polishMatAffect(0.5f);
        for (int i = 0; i < texNonPolished.Length; i++)
        {
            //texNonPolished[i] = 
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PolishItemChoose(int index)
    {
        for (int i = 0; i < ItemsToPolish.Length; i++)
        {
            ItemsToPolish[i].SetActive(false);
            if(i == index)
            {
                ItemsToPolish[i].SetActive(true);
                ItemsToPolish[i].GetComponentInChildren<Renderer>().material = polishMat;
                polishMat.SetTexture("_MainTex", itemData.texNonPolished[i]);
                polishMat.SetTexture("_MainTex2", itemData.texPolished[i]);
            }
        }
    }
    public void polishMatAffect(float f)
    {
        if (polishMat)
        {
            polishMat.SetFloat("_Blending", f);
        }
    }
}
