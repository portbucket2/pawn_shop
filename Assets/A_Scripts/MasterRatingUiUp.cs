﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MasterRatingUiUp : MonoBehaviour
{
    public Image[] starFills;
    public float masterRating;

    public float ratevalueTotal;
    public float rateCount;


    public static MasterRatingUiUp instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        masterRating = PlayerPrefs.GetFloat("masterRating", 5f);
        
        //masterRating = 5;
        //RateWithStars(masterRating);
        //MasterRateFromSingleRating(masterRating);
        rateCount = GameManagement.instance.levelIndex + 1;
        ratevalueTotal = masterRating * (float)rateCount;
        RateWithStars(masterRating);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void MasterRateFromSingleRating(float rateSingle)
    {
        ratevalueTotal += rateSingle;
        rateCount += 1;
        if(rateCount > 0)
        {
            masterRating = ratevalueTotal / rateCount;
        }

        RateWithStars(masterRating);

        
    }

    public void RateWithStars(float rating)
    {
        int ratingInt = (int)rating;
        float frac = rating - ratingInt;
        for (int i = 0; i < starFills.Length; i++)
        {
            if(i < ratingInt)
            {
                starFills[i].fillAmount = 1;
            }
            else
            {
                starFills[i].fillAmount = 0;
            }
        }

        if(ratingInt < starFills.Length)
        {
            starFills[ratingInt].fillAmount = frac;
        }
        PlayerPrefs.SetFloat("masterRating", masterRating);
    }

}
